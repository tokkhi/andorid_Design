package com.io.tokkhi.popcorn;

import android.app.ProgressDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.VideoView;

public class Video_tri extends AppCompatActivity {
    Button btn;
    VideoView videoView = null;
    int currenttime = 0;
    String Url = "";
    private static ProgressDialog progressDialog;
    private static String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_tri);
        Bundle extras = getIntent().getExtras();
        if (null != extras) {
            Url = extras.getString("url");
            id = extras.getString("id");
        }
        progressDialog = ProgressDialog.show(this, "", "Loading...", true);
        videoView = (VideoView) findViewById(R.id.myvideoview);
        MediaController mediaController = new MediaController(this);
        mediaController.setAnchorView(videoView);
        Uri video = Uri.parse(Url);
        videoView.setMediaController(mediaController);
        videoView.setVideoURI(video);

        videoView.setOnPreparedListener(new OnPreparedListener() {

            public void onPrepared(MediaPlayer arg0) {
                progressDialog.dismiss();
                videoView.start();
                videoView.seekTo(currenttime);
            }
        });
        videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                Intent intent = new Intent(getApplicationContext(), detail_move.class);
                intent.putExtra("id", id);
                getApplicationContext().startActivity(intent);
                finish();
                return false;
            }
        });
    }

    @Override
    public void finish() {
        Intent data = new Intent();
        data.putExtra("currenttime", videoView.getCurrentPosition());
        setResult(RESULT_OK, data);
        super.finish();
    }
}
