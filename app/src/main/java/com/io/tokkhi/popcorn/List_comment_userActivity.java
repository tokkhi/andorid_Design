package com.io.tokkhi.popcorn;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.io.tokkhi.popcorn.API.CallServer;
import com.io.tokkhi.popcorn.API.api_link;
import com.io.tokkhi.popcorn.Adapter.CommentAdapter;
import com.io.tokkhi.popcorn.Adapter.Comment_UserAdapter;
import com.io.tokkhi.popcorn.model.Director;
import com.io.tokkhi.popcorn.model.Item;
import com.io.tokkhi.popcorn.model.Users;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class List_comment_userActivity extends AppCompatActivity {
    private RecyclerView mrecyclerView ;
    ArrayList<Users.Comment> mitem;
    private String Guid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment__more);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        SharedPreferences get = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        Guid = get.getString("Guid",null);
        api_link api = new CallServer().Call();
        Call<Users> call = api.Comment_User(new Users.Users_Input(Guid));
        call.enqueue(new Callback<Users>() {


            @Override
            public void onResponse(Call<Users> call, Response<Users> response) {
                mitem = response.body().getComment();
                mrecyclerView = findViewById(R.id.recycler_view);
                Comment_UserAdapter itemListDataAdapter = new Comment_UserAdapter(getApplicationContext(), mitem);
                mrecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                mrecyclerView.setAdapter(itemListDataAdapter);
            }

            @Override
            public void onFailure(Call<Users> call, Throwable t) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
