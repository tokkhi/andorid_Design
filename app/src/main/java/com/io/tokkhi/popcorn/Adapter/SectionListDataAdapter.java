package com.io.tokkhi.popcorn.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.io.tokkhi.popcorn.Detail_cast_dir;
import com.io.tokkhi.popcorn.R;
import com.io.tokkhi.popcorn.detail_move;
import com.io.tokkhi.popcorn.model.SingleItemModel;

import java.util.ArrayList;

import Helper.DownLoadImageTask;

/**
 * Created by Tokkhi on 2/15/2018.
 */

public class SectionListDataAdapter extends RecyclerView.Adapter<SectionListDataAdapter.SingleItemRowHolder> {

    private ArrayList<SingleItemModel> itemsList;
    private Context mContext;
    private String tag;

    public SectionListDataAdapter(Context context, ArrayList<SingleItemModel> itemsList,String tag) {
        this.itemsList = itemsList;
        this.mContext = context;
        this.tag = tag;
    }

    @Override
    public SingleItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_single_home, null);
        SingleItemRowHolder mh = new SingleItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(SingleItemRowHolder holder, int i) {

        SingleItemModel singleItem = itemsList.get(i);

        holder.tvTitle.setText(singleItem.getName());
        String url_img = mContext.getResources().getString(R.string.url_img) + singleItem.getimg();
        holder.itemImage.setVisibility(View.GONE);
        holder.pb.setVisibility(View.VISIBLE);
        new DownLoadImageTask(holder.itemImage,holder.pb).execute(url_img);
    }

    @Override
    public int getItemCount() {
        return (null != itemsList ? itemsList.size() : 0);
    }

    public class SingleItemRowHolder extends RecyclerView.ViewHolder {

        protected TextView tvTitle;

        protected ImageView itemImage;
        public ProgressBar pb;


        public SingleItemRowHolder(View view) {
            super(view);

            this.tvTitle = (TextView) view.findViewById(R.id.tvTitle);
            this.itemImage = (ImageView) view.findViewById(R.id.itemImage);
            this.pb = view.findViewById(R.id.progressBar1);


            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = null;
                    switch (tag){
                        case "Movie":
                            intent = new Intent(v.getContext(), detail_move.class);
                            intent.putExtra("id", itemsList.get(getAdapterPosition()).getid());
                            break;
                        case "Caster":
                            intent = new Intent(v.getContext(), Detail_cast_dir.class);
                            intent.putExtra("id", itemsList.get(getAdapterPosition()).getid());
                            intent.putExtra("tag", "Caster");
                            break;
                        case "Director":
                            intent = new Intent(v.getContext(), Detail_cast_dir.class);
                            intent.putExtra("id", itemsList.get(getAdapterPosition()).getid());
                            intent.putExtra("tag", "Director");
                            break;
                    }
                    v.getContext().startActivity(intent);
                    Toast.makeText(v.getContext(), tvTitle.getText(), Toast.LENGTH_SHORT).show();

                }
            });


        }

    }

}