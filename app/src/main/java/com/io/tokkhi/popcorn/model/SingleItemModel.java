package com.io.tokkhi.popcorn.model;

/**
 * Created by Tokkhi on 2/15/2018.
 */

public class SingleItemModel {
    private String name;
    private String id;
    private String img;


    public SingleItemModel() {
    }

    public SingleItemModel(String name, String id ,String img) {
        this.name = name;
        this.id = id;
        this.img = img;
    }


    public String getid() {
        return id;
    }

    public void setid(String url) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getimg() {
        return img;
    }

    public void setimg(String img) {
        this.img = img;
    }


}
