package com.io.tokkhi.popcorn;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.io.tokkhi.popcorn.API.CallServer;
import com.io.tokkhi.popcorn.API.api_link;
import com.io.tokkhi.popcorn.model.Login_input;
import com.io.tokkhi.popcorn.model.Login_output;
import com.io.tokkhi.popcorn.model.Review;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class comment extends AppCompatActivity {

    private Button bt_com;
    private EditText et_com;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.comment);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        SharedPreferences get = PreferenceManager.getDefaultSharedPreferences(this);
        Intent intent = getIntent();
        final String id = intent.getStringExtra("id");
        final String Guid = get.getString("Guid",null);
        final String app = getResources().getString(R.string.Key_app);
        bt_com = (Button) findViewById(R.id.bt_com);
        et_com = (EditText) findViewById(R.id.et_com);
        bt_com.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_com.getText().toString() != null || et_com.getText().toString().length() > 1) {
                    api_link api = new CallServer().Call();
                    Call<Review.output> call = api.Review_Comment(new Review.Comment(Guid, et_com.getText().toString(), id, app));
                    call.enqueue(new Callback<Review.output>() {
                        @Override
                        public void onResponse(Call<Review.output> call, Response<Review.output> response) {
                            Review.output output = response.body();
                            if (output.getReturn_message().equals("Success")) {
                                Toast.makeText(comment.this, "Success", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(), detail_move.class);
                                intent.putExtra("id", id);
                                getApplicationContext().startActivity(intent);
                            }
                            if (output.getReturn_message().equals("alrady")) {
                                Toast.makeText(comment.this, "already", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(comment.this, "output : " + output.getReturn_message(), Toast.LENGTH_SHORT).show();
                            }

                        }

                        @Override
                        public void onFailure(Call<Review.output> call, Throwable t) {

                        }
                    });
                }else{
                    Toast.makeText(comment.this, "ยังไม่ได้มีการคอมเม้น", Toast.LENGTH_SHORT).show();
                }
            }

        });
    }




    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
