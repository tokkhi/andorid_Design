package com.io.tokkhi.popcorn;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.io.tokkhi.popcorn.API.CallServer;
import com.io.tokkhi.popcorn.API.api_link;
import com.io.tokkhi.popcorn.model.Review;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Edit_CommentActivity extends AppCompatActivity {

    private Button bt_com;
    private EditText et_com;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.comment);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        SharedPreferences get = PreferenceManager.getDefaultSharedPreferences(this);
        Intent intent = getIntent();
        final String id_m = intent.getStringExtra("id_m");
        final String id_c = intent.getStringExtra("id_c");
        final String text = intent.getStringExtra("text");
        final String Guid = get.getString("Guid",null);
        final String app = getResources().getString(R.string.Key_app);
        bt_com = (Button) findViewById(R.id.bt_com);
        et_com = (EditText) findViewById(R.id.et_com);
        et_com.setText(text);
        bt_com.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                api_link api = new CallServer().Call();
                Call<Review.output> call = api.Edit_Comment(new Review.Input_Comment_Edit(Guid,id_c,et_com.getText().toString()));
                call.enqueue(new Callback<Review.output>() {
                    @Override
                    public void onResponse(Call<Review.output> call, Response<Review.output> response) {
                        Review.output output = response.body();
                        if (output.getReturn_message().equals("Success")){
                            Toast.makeText(getApplicationContext(), "Success", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getApplicationContext(), detail_move.class);
                            intent.putExtra("id", id_m);
                            getApplicationContext().startActivity(intent);
                        }
                        if (output.getReturn_message().equals("alrady")){
                            Toast.makeText(getApplicationContext(), "already", Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(getApplicationContext(), "output : "+output.getReturn_message(), Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<Review.output> call, Throwable t) {

                    }
                });
            }


        });
    }




    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
