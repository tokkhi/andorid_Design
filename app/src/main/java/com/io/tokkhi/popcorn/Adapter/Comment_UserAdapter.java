package com.io.tokkhi.popcorn.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ahmadrosid.svgloader.SvgLoader;
import com.io.tokkhi.popcorn.API.CallServer;
import com.io.tokkhi.popcorn.API.api_link;
import com.io.tokkhi.popcorn.Edit_CommentActivity;
import com.io.tokkhi.popcorn.R;
import com.io.tokkhi.popcorn.model.Item;
import com.io.tokkhi.popcorn.model.Review;
import com.io.tokkhi.popcorn.model.Users;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Tokkhi on 4/30/2018.
 */

public class Comment_UserAdapter extends RecyclerView.Adapter<Comment_UserAdapter.Holder> {

    private final ArrayList<Users.Comment> itemsList;
    private Context mContext;
    private LayoutInflater mInflater;
    private Item.Comment item;
    private int size;
    private int position;

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }



    public class Holder extends RecyclerView.ViewHolder  implements View.OnCreateContextMenuListener {

        protected TextView tvTitle;
        protected TextView date;
        protected TextView tvcom;
        protected TextView tv_like;



        public Holder(View view) {
            super(view);

            this.tvTitle = (TextView) view.findViewById(R.id.tv_namemovie);
            this.date = view.findViewById(R.id.date);
            this.tvcom = view.findViewById(R.id.comment);
            this.tv_like = view.findViewById(R.id.tv_like);
            view.setOnCreateContextMenuListener(this);

        }



        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            SharedPreferences get = PreferenceManager.getDefaultSharedPreferences(mContext);
            final String Guid = get.getString("Guid",null);

            if(Guid != null){
                    menu.add("ลบ").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {

                            api_link api = new CallServer().Call();
                            Call<Review.output> call = api.Delete_Comment(new Review.Input_Delete(itemsList.get(getAdapterPosition()).getId()));
                            call.enqueue(new Callback<Review.output>() {
                                @Override
                                public void onResponse(Call<Review.output> call, Response<Review.output> response) {
                                    Review.output output = response.body();
                                    if (output.getReturn_message().equals("Success")){
                                        removeAt(getAdapterPosition());
                                        Toast.makeText(mContext, R.string.Success, Toast.LENGTH_SHORT).show();
                                    }
                                }
                                @Override
                                public void onFailure(Call<Review.output> call, Throwable t) {

                                }
                            });
                            return true;
                        }
                    });
                    menu.add("แก้ไข").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            Toast.makeText(mContext, "แก้ไข"+itemsList.get(getAdapterPosition()).getComment() , Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(mContext, Edit_CommentActivity.class);
                            intent.putExtra("id_m", itemsList.get(getAdapterPosition()).getId_m());
                            intent.putExtra("text",itemsList.get(getAdapterPosition()).getComment());
                            intent.putExtra("id_c", itemsList.get(getAdapterPosition()).getId());
                            itemView.getContext().startActivity(intent);
                            return true;
                        }
                    });
            }

        }

    }

    public Comment_UserAdapter(Context context, ArrayList<Users.Comment> itemsList) {
        super();
        this.itemsList = itemsList;
        this.mContext = context;
    }




    @Override
    public Comment_UserAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.item_comment_user, parent, false);
        return  new Comment_UserAdapter.Holder(itemView);
    }

    @Override
    public void onBindViewHolder(final Comment_UserAdapter.Holder holder, int position) {
        final Users.Comment mItem = itemsList.get(position);

        holder.tvcom.setText(mItem.getComment());

        holder.tvTitle.setText(mItem.getName_Movie());
        holder.tv_like.setText(mItem.getLike());
        holder.date.setText(mItem.getDate());


    }

    @Override
    public int getItemCount() {

            return itemsList.size();


    }
    public void removeAt(int position) {
        itemsList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, itemsList.size());
    }
}
