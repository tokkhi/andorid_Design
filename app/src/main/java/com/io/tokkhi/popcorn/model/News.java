package com.io.tokkhi.popcorn.model;

import java.util.ArrayList;

/**
 * Created by Tokkhi on 4/14/2018.
 */

public class News {
    private ArrayList<item> News;

    public ArrayList<item> getNews() {
        return News;
    }

    public void setNews(ArrayList<item> news) {
        News = news;
    }

    public class item{
    private String ID;
    private String Name;
    private String url;
    private String date;

        public String getID() {
            return ID;
        }

        public void setID(String ID) {
            this.ID = ID;
        }

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }
    }
}
