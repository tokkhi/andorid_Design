package com.io.tokkhi.popcorn;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class AddMovieActivity extends AppCompatActivity {

    private WebView myWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_movie);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        myWebView = (WebView) findViewById(R.id.Add_Movie);
        // myWebView = new WebView(this);
       String url = "http://203.158.131.68/Popcorn/Login/Users";


        myWebView.loadUrl(url);

        myWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onLoadResource(WebView view, String url)
            {
                myWebView.loadUrl("javascript:(function() { " +
                        "document.getElementById('top-nav').style.display='none';})()");
                myWebView.loadUrl("javascript:(function() { " +
                        "document.getElementById('menu').style.display='none';})()");

            }
            public boolean shouldOverrideUrlLoading(WebView view, String url)
            {

                //do your stuff here
                if(url.equalsIgnoreCase("http://203.158.131.68/Popcorn/Movie"))
                {
                    Toast.makeText(getApplicationContext(), "false" ,
                            Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(getApplicationContext(), User.class);
                    getApplicationContext().startActivity(intent);

                    return true;
                }

                Toast.makeText(getApplicationContext(), "true" ,
                        Toast.LENGTH_LONG).show();
                return false;
            }
        });
        WebSettings webSettings = myWebView.getSettings();

        webSettings.setJavaScriptEnabled(true);



    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
