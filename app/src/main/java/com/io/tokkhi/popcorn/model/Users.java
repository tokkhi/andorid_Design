package com.io.tokkhi.popcorn.model;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class Users {
    private String Name;
    private String Email;
    private String img;
    private String Type;
    private String Password;
    @SerializedName("comment")
    @Expose
    private ArrayList<Comment> Comment;
    public Users(String name){
        this.Name = name;
    }
    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public ArrayList<Users.Comment> getComment() {
        return Comment;
    }

    public void setComment(ArrayList<Users.Comment> comment) {
        Comment = comment;
    }

    public static class Users_Input{
        private String ID_User;
        public Users_Input(String ID_User){
            this.ID_User = ID_User;
        }

        public String getInput() {
            return ID_User;
        }

        public void setInput(String input) {
            this.ID_User = ID_User;
        }
    }
    public static class Edit_Input{
        private String Name;
        private String ID_User;
        private String img;

        public Edit_Input(String ID_User,String Name,String img){
            this.ID_User = ID_User;
            this.Name = Name;
            this.img = img;

        }



        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public String getID_User() {
            return ID_User;
        }

        public void setID_User(String ID_User) {
            this.ID_User = ID_User;
        }
    }
    public static class Edit_Pass{
        private String ID_User;
        private String Password;

        public Edit_Pass(String ID_User,String password){
            this.ID_User = ID_User;
            this.Password = password;
        }
        public String getID_User() {
            return ID_User;
        }

        public void setID_User(String ID_User) {
            this.ID_User = ID_User;
        }

        public String getPassword() {
            return Password;
        }

        public void setPassword(String password) {
            Password = password;
        }
    }
    public static class Comment{
        @SerializedName("ID_Comment")
        @Expose
        private String id;
        @SerializedName("ID_Movie")
        @Expose
        private String id_m;
        @SerializedName("Name_Movie")
        @Expose
        private String Name_Movie;
        @SerializedName("Date")
        @Expose
        private String date;
        @SerializedName("text")
        @Expose
        private String comment;
        @SerializedName("Count_Like")
        @Expose
        private String like;



        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getId_m() {
            return id_m;
        }

        public void setId_m(String id_m) {
            this.id_m = id_m;
        }

        public String getLike() {
            return like;
        }

        public void setLike(String like) {
            this.like = like;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public String getName_Movie() {
            return Name_Movie;
        }

        public void setName_Movie(String name_Movie) {
            Name_Movie = name_Movie;
        }
    }
}
