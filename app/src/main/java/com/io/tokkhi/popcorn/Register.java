package com.io.tokkhi.popcorn;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.io.tokkhi.popcorn.API.CallServer;
import com.io.tokkhi.popcorn.API.api_link;
import com.io.tokkhi.popcorn.model.Register.Register_output;
import com.io.tokkhi.popcorn.model.Register.Register_input;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Register extends AppCompatActivity {

    private Button submit;
    private EditText etName;
    private EditText etEmail;
    private EditText etPass;
    private EditText etPasscon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        submit = (Button) findViewById(R.id.bt_regis);
        etName = (EditText) findViewById(R.id.et_name);
        etEmail = (EditText) findViewById(R.id.et_Email);
        etPass = (EditText) findViewById(R.id.et_Password);
        etPasscon = (EditText) findViewById(R.id.et_Passwordcon);
        final String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etEmail.getText().toString().matches(emailPattern)&& etEmail.getText().length() > 0){

                }else{  etEmail.setError("อีเมล์ไม่ถูกต้อง"); } if(etPass.getText().length() < 8){
                    etPass.setError("ใส่รหัสอย่างน้อบ 8 ตัว");
                }else if(!etPass.getText().toString().equals(etPasscon.getText().toString())){
                    etPasscon.setError("รหัสผ่านไม่ตรงกัน");
                }else {
                    api_link api = new CallServer().Call();
                    Call<Register_output> call = api.Register(new Register_input(etEmail.getText().toString(),etPasscon.getText().toString(),etName.getText().toString(),"2"));
                    call.enqueue(new Callback<Register_output>() {
                        @Override
                        public void onResponse(Call<Register_output> call, Response<Register_output> response) {
                            String msg = response.body().getReturn_message();
                            if(msg.equals("Success")){
                                Intent myIntent = new Intent(getApplicationContext(), Login.class);
                                // myIntent.putExtra("key", value);
                                getApplication().startActivity(myIntent);
                            }
                        }
                        @Override
                        public void onFailure(Call<Register_output> call, Throwable t) {

                        }


                    });
                }



            }
        });
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
