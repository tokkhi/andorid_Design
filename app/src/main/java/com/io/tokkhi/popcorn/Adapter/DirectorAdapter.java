package com.io.tokkhi.popcorn.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.io.tokkhi.popcorn.Detail_cast_dir;
import com.io.tokkhi.popcorn.R;
import com.io.tokkhi.popcorn.model.Director;

import java.util.ArrayList;

import Helper.DownLoadImageTask;

public class DirectorAdapter extends RecyclerView.Adapter<DirectorAdapter.ViewHolder> {

    private static ArrayList<Director.item_list> mData;
    private Context mContext;
    private Director.item_list item;

    public static class ViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener {

        public TextView mName;
        public ImageView mImage;
        public ProgressBar pb;


        public ViewHolder(View view) {
            super(view);
            mName = (TextView) view.findViewById(R.id.tvName);
            mImage = (ImageView) view.findViewById(R.id.itemImage);
            pb= view.findViewById(R.id.progressBar1);
            view.setOnClickListener(this);


        }
        private enum type {
            Caster, Director
        }
        @Override
        public void onClick(View v) {
            Intent intent = null;

            intent = new Intent(v.getContext(), Detail_cast_dir.class);

            intent.putExtra("id", mData.get(getAdapterPosition()).getId());
            intent.putExtra("tag", "Director");
            v.getContext().startActivity(intent);


        }
    }

    public DirectorAdapter(Context context, ArrayList<Director.item_list> Data) {
        mData = Data;
        mContext = context;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.item_movie, parent, false);
        return  new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        item = mData.get(position);

        viewHolder.mName.setText(item.getName());
        String url_img = mContext.getResources().getString(R.string.url_img) + item.getImg();
        viewHolder.mImage.setVisibility(View.GONE);
        viewHolder.pb.setVisibility(View.VISIBLE);
        new DownLoadImageTask(viewHolder.mImage,viewHolder.pb).execute(url_img);

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }



}