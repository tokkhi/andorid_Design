package com.io.tokkhi.popcorn;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;

import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import android.support.v4.view.ViewPager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.ahmadrosid.svgloader.SvgLoader;
import com.io.tokkhi.popcorn.API.CallServer;
import com.io.tokkhi.popcorn.API.api_link;

import com.io.tokkhi.popcorn.model.Search;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.io.tokkhi.popcorn.R.layout.*;

public class MainActivity extends AppCompatActivity

        implements NavigationView.OnNavigationItemSelectedListener {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private LinearLayout mnav_header;
    private SimpleCursorAdapter myAdapter;
    private SearchView searchView;

    private ArrayList<Search> SearchList =new ArrayList<>();
    private Cursor mCursor;
    private static final String TAG = "MainActivity";
    private TextView mName;
    private String name =null;
    private ImageView mImage;
    private String img;
    private TextView mtype;
    private String type;


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme_NoActionBar);
        super.onCreate(savedInstanceState);
        setContentView(activity_main);
        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer =  findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        AtomicReference<NavigationView> navigationView = new AtomicReference<>((NavigationView) findViewById(R.id.nav_view));
        navigationView.get().setNavigationItemSelectedListener(this);

        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewPager =  findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout =  findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        final String[] from = new String[] {"Name"};
        final int[] to = new int[] {R.id.name};

        myAdapter = new SimpleCursorAdapter(MainActivity.this,
                R.layout.list_search, null, from, to, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create channel to show notifications.
            String channelId  = getString(R.string.default_notification_channel_id);
            String channelName = getString(R.string.default_notification_channel_name);
            NotificationManager notificationManager =
                    getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(new NotificationChannel(channelId,
                    channelName, NotificationManager.IMPORTANCE_LOW));
        }

        if (getIntent().getExtras() != null) {
            for (String key : getIntent().getExtras().keySet()) {
                Object value = getIntent().getExtras().get(key);
                Log.d(TAG, "Key: " + key + " Value: " + value);
            }
        }
        api_link api = new CallServer().Call();
        Call<ArrayList<Search>> call = api.search();
        call.enqueue(new Callback<ArrayList<Search>>() {

            @Override
            public void onResponse(Call<ArrayList<Search>> call, Response<ArrayList<Search>> response) {
                SearchList = response.body();
            }

            @Override
            public void onFailure(Call<ArrayList<Search>> call, Throwable t) {

            }
        });
    }




    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main, menu);

        SharedPreferences  get = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        name = get.getString("Name",null);
        img = get.getString("img",null);
        type = get.getString("type",null);
        mName =  findViewById(R.id.name);
        mImage =  findViewById(R.id.imageView);
        mtype =  findViewById(R.id.type);
        if(TextUtils.isEmpty(name)){

        }else{
            mName.setText(name);
            switch (type){
                case "1":
                    mtype.setText("admin");
                    break;
                case "2":
                    mtype.setText(getResources().getString(R.string.user));
                    break;
                case "3":
                    mtype.setText(getResources().getString(R.string.master));
                    break;
                    default:
                        mtype.setText(getResources().getString(R.string.user));
                        break;
            }

            if(img != null){
                String url = getResources().getString(R.string.Url_svg) + img;
                SvgLoader.pluck()
                        .with(this)
                        .setPlaceHolder(R.mipmap.ic_launcher, R.mipmap.ic_launcher)
                        .load(url, mImage);
            }

        }
        mnav_header =  findViewById(R.id.nav_header);

        final String finalName = name;
        if(mnav_header != null){
            mnav_header.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent myIntent = null;
                    if(name != null){
                        myIntent = new Intent(MainActivity.this, User.class);
                    }else{
                        myIntent = new Intent(MainActivity.this, Login.class);
                    }

                    // myIntent.putExtra("key", value);
                    MainActivity.this.startActivity(myIntent);

                }
            });
        }


        MenuItem searchItem = menu.findItem(R.id.search);
        SearchManager searchManager = (SearchManager) MainActivity.this.getSystemService(Context.SEARCH_SERVICE);
        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(MainActivity.this.getComponentName()));
            searchView.setIconified(false);
            searchView.setSuggestionsAdapter(myAdapter);
            // Getting selected (clicked) item suggestion
            searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
                @Override
                public boolean onSuggestionClick(int position) {

                    // Add clicked text to search box
                    CursorAdapter ca = searchView.getSuggestionsAdapter();
                    Cursor cursor = ca.getCursor();
                    cursor.moveToPosition(position);
                    searchView.setQuery(cursor.getString(cursor.getColumnIndex("Name")),false);
                    String name =  cursor.getString(cursor.getColumnIndex("Name"));
                    String id =  cursor.getString(cursor.getColumnIndex("id"));
                    String tag =  cursor.getString(cursor.getColumnIndex("Tag"));
                    switch (tag)
                    {
                        case "Movie":{
                            Intent intent = new Intent(getApplicationContext(), detail_move.class);
                            intent.putExtra("id",id);
                            startActivity(intent);
                            break;
                        }
                        case "Caster":{
                            Intent intent = new Intent(getApplicationContext(), Detail_cast_dir.class);
                            intent.putExtra("id",id);
                            intent.putExtra("tag","Caster");
                            startActivity(intent);
                            break;
                        }
                        case "Director":{
                            Intent intent = new Intent(getApplicationContext(), Detail_cast_dir.class);
                            intent.putExtra("id",id);
                            intent.putExtra("tag","Director");

                            startActivity(intent);
                            break;
                        }
                        default:
                            break;

                    }

                    return true;
                }

                @Override
                public boolean onSuggestionSelect(int position) {
                    return true;
                }
            });
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String s) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String s) {
                    MatrixCursor cursor = new MatrixCursor(new String[] {"_id", "Name", "Tag","id","img"});
                    int rowId =0;
                    for ( Search item : SearchList )
                    {
                        if(item.getName_Eng() != null){
                            if (item.getName().toLowerCase().startsWith(s.toLowerCase())|| item.getName_Eng().toLowerCase().startsWith(s.toLowerCase())){
                                cursor.addRow(new Object[] {rowId, item.getName(),item.getStatus(),item.getID(),item.getImg()});
                            }

                        }else {
                            if (item.getName().toLowerCase().startsWith(s.toLowerCase())){
                                cursor.addRow(new Object[] {rowId, item.getName(),item.getStatus(),item.getID(),item.getImg()});
                            }
                        }

                        rowId++;
                    }
                    myAdapter.changeCursor(cursor);
                    return false;
                }
            });
        }

        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();



        if (id == R.id.commedy) {
            Intent intent = new Intent(this, GenreActivity.class);
            intent.putExtra("id","GE_00001");
            this.startActivity(intent);
        } else if (id == R.id.action) {
            Intent intent = new Intent(this, GenreActivity.class);
            intent.putExtra("id","GE_00002");
            this.startActivity(intent);

        } else if (id == R.id.scifi) {
            Intent intent = new Intent(this, GenreActivity.class);
            intent.putExtra("id","GE_00014");
            this.startActivity(intent);

        } else if (id == R.id.drama) {
            Intent intent = new Intent(this, GenreActivity.class);
            intent.putExtra("id","GE_00004");
            this.startActivity(intent);

        } else if (id == R.id.romantic) {
            Intent intent = new Intent(this, GenreActivity.class);
            intent.putExtra("id","GE_00005");
            this.startActivity(intent);

        } else if (id == R.id.horror) {
            Intent intent = new Intent(this, GenreActivity.class);
            intent.putExtra("id","GE_00006");
            this.startActivity(intent);

        }
        //Rating
        else if(id == R.id.General){
            Intent intent = new Intent(this, RateActivity.class);
            intent.putExtra("id","1");
            this.startActivity(intent);

        } else if(id == R.id.PG_13){
            Intent intent = new Intent(this, RateActivity.class);
            intent.putExtra("id","2");
            this.startActivity(intent);

        } else if(id == R.id.NC_17){
            Intent intent = new Intent(this, RateActivity.class);
            intent.putExtra("id","3");
            this.startActivity(intent);

        } else if(id == R.id.Restricted){
            Intent intent = new Intent(this, RateActivity.class);
            intent.putExtra("id","4");
            this.startActivity(intent);

        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    public void setupViewPager(ViewPager upViewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new HomeActivity(), "หน้าแรก");
        adapter.addFragment(new movie(), "ภาพยนตร์");
        adapter.addFragment(new news(), "ข่าว");
        adapter.addFragment(new CasterActivity(), getResources().getString(R.string.Caster));
        adapter.addFragment(new DirectorActivity(), getResources().getString(R.string.Directer));
        viewPager.setAdapter(adapter);
    }


     class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

         public ViewPagerAdapter(FragmentManager fm) {
             super(fm);
         }

         public void addFragment(Fragment oneFragment, String one) {
             mFragmentList.add(oneFragment);
             mFragmentTitleList.add(one);
        }

         @Override
         public Fragment getItem(int position) {
             return mFragmentList.get(position);
         }

         @Override
         public int getCount() {
             return mFragmentList.size();
         }

         @Override
         public CharSequence getPageTitle(int position) {
             return mFragmentTitleList.get(position);
         }
     }


}

