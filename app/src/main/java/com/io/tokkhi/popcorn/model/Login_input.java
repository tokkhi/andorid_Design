package com.io.tokkhi.popcorn.model;

/**
 * Created by Tokkhi on 2/25/2018.
 */

public class Login_input {
    private String email;
    private String password;
    private String Device_ID;

    public Login_input(String email, String password,String Device_ID) {
        this.email = email;
        this.password = password;
        this.Device_ID = Device_ID;
    }


    /**
     *
     * @return
     * The email
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     * The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return
     * The password
     */
    public String getPassword() {
        return password;
    }

    /**
     *
     * @param password
     * The password
     */
    public void setPassword(String password) {
        this.password = password;
    }



    public String getDevice_ID() {
        return Device_ID;
    }

    public void setDevice_ID(String device_ID) {
        Device_ID = device_ID;
    }
}
