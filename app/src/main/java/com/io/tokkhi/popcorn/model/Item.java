package com.io.tokkhi.popcorn.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Tokkhi on 2/24/2018.
 */

public class Item implements Serializable {
    public class movie_list{
        @SerializedName("ID_Movie")
        @Expose
        private String id;

        @SerializedName("Name_Movie")
        @Expose
        private String name;
        @SerializedName("img")
        @Expose
        private String img;
        @SerializedName("Caster")
        @Expose
        private ArrayList<movie_Detail> Caster = new ArrayList<>();
        /**
         * @return The id
         */
        public String getId() {
            return id;
        }

        /**
         * @param id The id
         */
        public void setId(String id) {
            this.id = id;
        }


        public void setName(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public String getimg() {
            return img;
        }

        public void setimg(String img) {
            this.img = img;
        }

        public ArrayList<movie_Detail> getCaster() {
            return Caster;
        }

        public void setCaster(ArrayList<movie_Detail> caster) {
            Caster = caster;
        }
    }

    public class movie_Detail{
        @SerializedName("ID_Movie")
        @Expose
        private String id;

        @SerializedName("Name_Movie")
        @Expose
        private String name;

        @SerializedName("Date_Movie")
        @Expose
        private String Date_Movie;

        @SerializedName("detail")
        @Expose
        private String Detail;

        @SerializedName("img")
        @Expose
        private String img;

        @SerializedName("Studio")
        @Expose
        private String Studio;

        @SerializedName("Studio_Url")
        @Expose
        private String Studio_Url;

        @SerializedName("Trialer")
        @Expose
        private String Trialer;
        @SerializedName("Caster")
        @Expose
        private ArrayList<Caster>Caster;
        @SerializedName("Director")
        @Expose
        private ArrayList<Director>Director;
        @SerializedName("Genre")
        @Expose
        private ArrayList<Genre>Genre;
        @SerializedName("Rate")
        @Expose
        private String Rate;
        @SerializedName("Score_Master")
        @Expose
        private Double Score_Master;
        @SerializedName("Score_User")
        @Expose
        private Double Score_User;
        @SerializedName("Comment_User")
        @Expose
        private ArrayList<Item.Comment>Comment_User;
        @SerializedName("Comment_Master")
        @Expose
        private ArrayList<Item.Comment>Comment_Master;
        @SerializedName("Check_Comment")
        @Expose
        private Boolean check_Comment;

        /**
         * @return The id
         */
        public String getId() {
            return id;
        }

        /**
         * @param id The id
         */
        public void setId(String id) {
            this.id = id;
        }

        /**
         * @return The name
         */
        public String getName() {
            return name;
        }

        /**
         * @param name The name
         */
        public void setName(String name) {
            this.name = name;
        }

        public void setDate_Movie(String date_Movie) {
            Date_Movie = date_Movie;
        }

        public String getDate_Movie() {
            return Date_Movie;
        }

        public String getDetail() {
            return Detail;
        }

        public void setDetail(String detail) {
            Detail = detail;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public String getStudio() {
            return Studio;
        }

        public void setStudio(String studio) {
            Studio = studio;
        }

        public String getStudio_Url() {
            return Studio_Url;
        }

        public void setStudio_Url(String studio_Url) {
            Studio_Url = studio_Url;
        }

        public String getTrialer() {
            return Trialer;
        }

        public void setTrialer(String trialer) {
            Trialer = trialer;
        }

        public ArrayList<Caster> getCaster() {
            return Caster;
        }

        public void setCaster(ArrayList<Caster> caster) {
            Caster = caster;
        }

        public ArrayList<Item.Director> getDirector() {
            return Director;
        }

        public void setDirector(ArrayList<Item.Director> director) {
            Director = director;
        }

        public ArrayList<Genre> getGenre() {
            return Genre;
        }

        public void setGenre(ArrayList<Genre> genre) {
            Genre = genre;
        }

        public String getRate() {
            return Rate;
        }

        public void setRate(String rate) {
            Rate = rate;
        }

        public Double getScore_Master() {
            return Score_Master;
        }

        public void setScore_Master(Double score_Master) {
            Score_Master = score_Master;
        }

        public Double getScore_User() {
            return Score_User;
        }

        public void setScore_User(Double score_User) {
            Score_User = score_User;
        }

        public ArrayList<Comment> getComment_User() {
            return Comment_User;
        }

        public void setComment_User(ArrayList<Comment> comment_User) {
            Comment_User = comment_User;
        }

        public ArrayList<Comment> getComment_Master() {
            return Comment_Master;
        }

        public void setComment_Master(ArrayList<Comment> comment_Master) {
            Comment_Master = comment_Master;
        }

        public Boolean getCheck_Comment() {
            return check_Comment;
        }

        public void setCheck_Comment(Boolean check_Comment) {
            this.check_Comment = check_Comment;
        }
    }
    public class Caster{
        @SerializedName("ID_Caster")
        @Expose
        private String id;
        @SerializedName("Name_Lastname_Caster")
        @Expose
        private String name;
        @SerializedName("Picture_Caster")
        @Expose
        private String img;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }
    }
    public class Director{
        @SerializedName("ID_Director")
        @Expose
        private String id;
        @SerializedName("Name")
        @Expose
        private String name;
        @SerializedName("Picture_Director")
        @Expose
        private String img;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }
    }
    public class Genre{
        @SerializedName("ID_Genre")
        @Expose
        private String id;
        @SerializedName("Name_Genre")
        @Expose
        private String name;


        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }
    public static class Comment implements Serializable{
        @SerializedName("ID_Comment")
        @Expose
        private String id;
        @SerializedName("ID_Movie")
        @Expose
        private String id_m;
        @SerializedName("Guid")
        @Expose
        private String id_u;
        @SerializedName("Name")
        @Expose
        private String name;
        @SerializedName("Date")
        @Expose
        private String date;
        @SerializedName("img")
        @Expose
        private String img;
        @SerializedName("Comment_Text")
        @Expose
        private String comment;
        @SerializedName("Count_Like")
        @Expose
        private String like;
        @SerializedName("check")
        @Expose
        private Boolean check;


        public Comment(String id, String name, String date,String comment,String like){
            this.id= id;
            this.name = name;
             this.date = date;
             this.comment = comment;
             this.like = like;
        }



        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public String getLike() {
            return like;
        }

        public void setLike(String like) {
            this.like = like;
        }

        public String getId_m() {
            return id_m;
        }

        public void setId_m(String id_m) {
            this.id_m = id_m;
        }

        public String getId_u() {
            return id_u;
        }

        public void setId_u(String id_u) {
            this.id_u = id_u;
        }


        public Boolean getCheck() {
            return check;
        }

        public void setCheck(Boolean check) {
            this.check = check;
        }
    }



}


