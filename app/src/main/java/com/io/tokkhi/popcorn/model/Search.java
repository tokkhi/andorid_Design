package com.io.tokkhi.popcorn.model;

public class Search {
    private String ID;
    private String Name;
    private String Status;
    private String img;
    private String Name_Eng;



    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getName_Eng() {
        return Name_Eng;
    }

    public void setName_Eng(String name_Eng) {
        Name_Eng = name_Eng;
    }
}
