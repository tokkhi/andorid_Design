package com.io.tokkhi.popcorn.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.io.tokkhi.popcorn.Detail_cast_dir;
import com.io.tokkhi.popcorn.R;
import com.io.tokkhi.popcorn.model.Caster;

import java.util.ArrayList;

import Helper.DownLoadImageTask;

/**
 * Created by Tokkhi on 3/1/2018.
 */

public class CasterAdapter extends RecyclerView.Adapter<CasterAdapter.ViewHolder> {

    private static ArrayList<Caster.item_list> mData;
    private Context mContext;
    private Caster.item_list item;

    public static class ViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener {

        public TextView mName;
        public ImageView mImage;
        public ProgressBar pb;


        public ViewHolder(View view) {
            super(view);
            mName = (TextView) view.findViewById(R.id.tvName);
            mImage = (ImageView) view.findViewById(R.id.itemImage);
            pb = view.findViewById(R.id.progressBar1);
            view.setOnClickListener(this);


        }
        private enum type {
            Caster, Director
        }
        @Override
        public void onClick(View v) {
            Intent intent = null;

                    intent = new Intent(v.getContext(), Detail_cast_dir.class);

            intent.putExtra("id", mData.get(getAdapterPosition()).getId());
            intent.putExtra("tag", "Caster");
            v.getContext().startActivity(intent);
            Toast.makeText(v.getContext(), "Name = " + mData.get(getAdapterPosition()).getName(), Toast.LENGTH_SHORT).show();

        }
    }

    public CasterAdapter(Context context, ArrayList<Caster.item_list> Data) {
        mData = Data;
        mContext = context;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.item_movie, parent, false);
        return  new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        item = mData.get(position);
        viewHolder.mName.setText(item.getName());
        String url_img = mContext.getResources().getString(R.string.url_img) + item.getImg();
        viewHolder.mImage.setVisibility(View.GONE);
        viewHolder.pb.setVisibility(View.VISIBLE);
        new DownLoadImageTask(viewHolder.mImage,viewHolder.pb).execute(url_img);

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }



}