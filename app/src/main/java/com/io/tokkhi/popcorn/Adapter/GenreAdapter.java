package com.io.tokkhi.popcorn.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.io.tokkhi.popcorn.R;
import com.io.tokkhi.popcorn.detail_move;
import com.io.tokkhi.popcorn.model.Director;
import com.io.tokkhi.popcorn.model.Item;
import com.io.tokkhi.popcorn.model.Movie;

import java.util.ArrayList;

import Helper.DownLoadImageTask;

/**
 * Created by Tokkhi on 4/19/2018.
 */

public class GenreAdapter extends RecyclerView.Adapter<GenreAdapter.ViewHolder>{

    private static ArrayList<Item.movie_list> mData;
    private final Context mContext;
    private Item.movie_list movie;

    public GenreAdapter(Context context, ArrayList<Item.movie_list> Data) {
        mData = Data;
        mContext = context;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener {

        public TextView mName;
        public ImageView mImage;
        public ProgressBar pb;

        public ViewHolder(View view) {
            super(view);
            mName = (TextView) view.findViewById(R.id.tvName);
            mImage = (ImageView) view.findViewById(R.id.itemImage);
            pb = view.findViewById(R.id.progressBar1);
            view.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {

            Intent intent = new Intent(v.getContext(), detail_move.class);
            intent.putExtra("id", mData.get(getAdapterPosition()).getId());
            v.getContext().startActivity(intent);
            Toast.makeText(v.getContext(), "Name = " + mData.get(getAdapterPosition()).getName(), Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    public GenreAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.item_genre, parent, false);

        return  new GenreAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(GenreAdapter.ViewHolder holder, int position) {
        movie = mData.get(position);

        holder.mName.setText(movie.getName().toString());
        String url_img = mContext.getResources().getString(R.string.url_img) + movie.getimg();
        holder.mImage.setVisibility(View.GONE);
        holder.pb.setVisibility(View.VISIBLE);
        new DownLoadImageTask(holder.mImage,holder.pb).execute(url_img);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }
}
