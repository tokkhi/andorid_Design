package com.io.tokkhi.popcorn;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.io.tokkhi.popcorn.API.CallServer;
import com.io.tokkhi.popcorn.API.api_link;
import com.io.tokkhi.popcorn.Adapter.HomeAdapter;
import com.io.tokkhi.popcorn.Adapter.MovieAdapter;
import com.io.tokkhi.popcorn.Adapter.Movie_DetailAdapter;
import com.io.tokkhi.popcorn.model.Item;
import com.io.tokkhi.popcorn.model.Login_input;
import com.io.tokkhi.popcorn.model.Login_output;
import com.io.tokkhi.popcorn.model.Movie;
import com.io.tokkhi.popcorn.model.SectionDataModel;
import com.io.tokkhi.popcorn.model.SingleItemModel;

import java.io.Serializable;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class detail_move extends AppCompatActivity {

    private ArrayList<Item.movie_Detail> details;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_move);
        SharedPreferences get = PreferenceManager.getDefaultSharedPreferences(this);

        final String Guid = get.getString("Guid",null);
        Intent intent = getIntent();
        String id = intent.getStringExtra("id");
        final Context context = this;
        api_link api = new CallServer().Call();
        Call<Movie> call = api.Movie_Detail(new Movie.Input(id,Guid));
        call.enqueue(new Callback<Movie>() {
            public Movie_DetailAdapter adapter;

            @Override
            public void onResponse(Call<Movie> call, Response<Movie> response) {
                details = response.body().getDetail_movie();
                recyclerView = findViewById(R.id.recycler_view_detail_movie);
                LinearLayoutManager manager = new LinearLayoutManager(context);
                recyclerView.setLayoutManager(manager);
                recyclerView.setHasFixedSize(true);
                adapter = new Movie_DetailAdapter(context,details);
                recyclerView.setAdapter(adapter);
            }

            @Override
                         public void onFailure(Call<Movie> call, Throwable t) {

                         }
                     });


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        finish();
        return super.onOptionsItemSelected(item);
    }
}
