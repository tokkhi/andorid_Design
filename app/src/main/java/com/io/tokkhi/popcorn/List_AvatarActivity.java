package com.io.tokkhi.popcorn;

import android.content.res.Resources;
import android.content.res.TypedArray;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.io.tokkhi.popcorn.Adapter.AvatarAdapter;
import com.io.tokkhi.popcorn.Adapter.Movie_DetailAdapter;

import java.util.ArrayList;

public class List_AvatarActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private AvatarAdapter adapter;
    ArrayList<String> list;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_list__avatar);
        list = new ArrayList<String>();
        addlist();
        recyclerView = findViewById(R.id.recycler_view);
        GridLayoutManager manager = new GridLayoutManager(this,3);
        recyclerView.setLayoutManager(manager);
        recyclerView.setHasFixedSize(true);
        adapter = new AvatarAdapter(this,list);
        recyclerView.setAdapter(adapter);
    }

    private void addlist() {

        for(int i=1;i<16;i++)
            list.add(i+".svg");

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
