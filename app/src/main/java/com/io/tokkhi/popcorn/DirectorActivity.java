package com.io.tokkhi.popcorn;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.io.tokkhi.popcorn.API.CallServer;
import com.io.tokkhi.popcorn.API.api_link;
import com.io.tokkhi.popcorn.Adapter.DirectorAdapter;
import com.io.tokkhi.popcorn.model.Caster;
import com.io.tokkhi.popcorn.model.Director;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DirectorActivity extends android.support.v4.app.Fragment {

    private ArrayList<Caster.item> contactList;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.director, container, false);
        final RecyclerView rv = (RecyclerView)v.findViewById(R.id.my_recycler_view);
        api_link api = new CallServer().Call();
        Call<Director> call = api.Director();
        contactList = new ArrayList<>();
        call.enqueue(new Callback<Director>() {
            @Override
            public void onResponse(Call<Director> call, Response<Director> response) {

                int numberOfColumns = 2;

                rv.setLayoutManager(new GridLayoutManager(getContext(),numberOfColumns));
                rv.setAdapter(new DirectorAdapter(getContext(),response.body().getDirector()));
            }

            @Override
            public void onFailure(Call<Director> call, Throwable t) {

            }
        });
        return v;

    }
}

