package com.io.tokkhi.popcorn.model;

/**
 * Created by Tokkhi on 4/14/2018.
 */

public class Review {

    public static class User{

        private String ID_User;
        private Float Score;
        private String ID_Movie;

        public User(String ID_User,Float score, String ID_Movie){
            this.ID_User = ID_User;
            this.ID_Movie = ID_Movie;
            this.Score = score;

        }

        public String getID_User() {
            return ID_User;
        }

        public void setID_User(String ID_User) {
            this.ID_User = ID_User;
        }

        public Float getScore() {
            return Score;
        }

        public void setScore(Float score) {
            Score = score;
        }

        public String getID_Movie() {
            return ID_Movie;
        }

        public void setID_Movie(String ID_Movie) {
            this.ID_Movie = ID_Movie;
        }
    }
    public static class Master{

        private String ID_User;
        private Float Score;
        private String ID_Movie;

        public Master(String ID_User,Float score, String ID_Movie){
            this.ID_User = ID_User;
            this.ID_Movie = ID_Movie;
            this.Score = score;

        }

        public String getID_User() {
            return ID_User;
        }

        public void setID_User(String ID_User) {
            this.ID_User = ID_User;
        }

        public Float getScore() {
            return Score;
        }

        public void setScore(Float score) {
            Score = score;
        }

        public String getID_Movie() {
            return ID_Movie;
        }

        public void setID_Movie(String ID_Movie) {
            this.ID_Movie = ID_Movie;
        }
    }
    public static class Comment{

        private String ID_User;
        private String Comment;
        private String ID_Movie;
        private String app_id;

        public Comment(String ID_User,String Comment, String ID_Movie,String appid){
            this.ID_User = ID_User;
            this.ID_Movie = ID_Movie;
            this.Comment = Comment;
            this.app_id = appid;

        }

        public String getID_User() {
            return ID_User;
        }

        public void setID_User(String ID_User) {
            this.ID_User = ID_User;
        }

        public String getID_Movie() {
            return ID_Movie;
        }

        public void setID_Movie(String ID_Movie) {
            this.ID_Movie = ID_Movie;
        }

        public String getComment() {
            return Comment;
        }

        public void setComment(String comment) {
            Comment = comment;
        }

        public String getAppid() {
            return app_id;
        }

        public void setAppid(String appid) {
            this.app_id = appid;
        }
    }
    public class output{
        private String return_message;

        public String getReturn_message() {
            return return_message;
        }

        public void setReturn_message(String return_message) {
            this.return_message = return_message;
        }

    }
    public static class Input_Like{
        private String ID_User;
        private String ID_Comment;
        public Input_Like (String ID_User,String ID_Comment){
            this.ID_User = ID_User;
            this.ID_Comment = ID_Comment;

        }

        public String getID_User() {
            return ID_User;
        }

        public void setID_User(String ID_User) {
            this.ID_User = ID_User;
        }

        public String getID_Comment() {
            return ID_Comment;
        }

        public void setID_Comment(String ID_Comment) {
            this.ID_Comment = ID_Comment;
        }
    }
    public static class Input_Report{
        private String ID_Comment;
        private String ID_User;

        public Input_Report(String ID_Comment,String ID_User){
            this.ID_Comment = ID_Comment;
            this.ID_User = ID_User;
        }

        public String getID_User() {
            return ID_User;
        }

        public void setID_User(String ID_User) {
            this.ID_User = ID_User;
        }

        public String getID_Comment() {
            return ID_Comment;
        }

        public void setID_Comment(String ID_Comment) {
            this.ID_Comment = ID_Comment;
        }
    }
    public  static  class Input_Delete{
        private String ID_Comment;

        public Input_Delete(String ID_Comment){
            this.ID_Comment = ID_Comment;
        }

        public String getID_Comment() {
            return ID_Comment;
        }

        public void setID_Comment(String ID_Comment) {
            this.ID_Comment = ID_Comment;
        }
    }
    public  static class Input_Comment_Edit{
        private String  ID_User;
        private String ID_Comment;
        private String Comment;

        public Input_Comment_Edit(String ID_User,String ID_Comment,String Comment){
            this.Comment = Comment;
            this.ID_Comment = ID_Comment;
            this.ID_User = ID_User;

        }

        public String getID_User() {
            return ID_User;
        }

        public void setID_User(String ID_User) {
            this.ID_User = ID_User;
        }

        public String getComment() {
            return Comment;
        }

        public void setComment(String comment) {
            Comment = comment;
        }

        public String getID_Comment() {
            return ID_Comment;
        }

        public void setID_Comment(String ID_Comment) {
            this.ID_Comment = ID_Comment;
        }
    }

}
