package com.io.tokkhi.popcorn;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.io.tokkhi.popcorn.API.CallServer;
import com.io.tokkhi.popcorn.API.api_link;
import com.io.tokkhi.popcorn.Adapter.GenreAdapter;
import com.io.tokkhi.popcorn.model.Item;
import com.io.tokkhi.popcorn.model.Movie;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Tokkhi on 4/30/2018.
 */

public class RateActivity extends AppCompatActivity {


    private RecyclerView rv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_genre);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Intent intent = getIntent();
        String id = intent.getStringExtra("id");
        rv = (RecyclerView) findViewById(R.id.recycler_view);
        api_link api = new CallServer().Call();
        Call<Movie> call = api.Movie_Rate(new Movie.Input_Genre(id));
        call.enqueue(new Callback<Movie>() {


            @Override
            public void onResponse(Call<Movie> call, Response<Movie> response) {
                ArrayList<Item.movie_list> singleSectionItems = response.body().getMovie_list();
                GenreAdapter itemListDataAdapter = new GenreAdapter(getApplicationContext(), singleSectionItems);
                rv.setHasFixedSize(true);
                rv.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
                rv.setAdapter(itemListDataAdapter);

            }

            @Override
            public void onFailure(Call<Movie> call, Throwable t) {

            }
        });
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}