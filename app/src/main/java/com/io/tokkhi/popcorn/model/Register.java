package com.io.tokkhi.popcorn.model;

/**
 * Created by Tokkhi on 2/28/2018.
 */

public class Register {

 public static class Register_input{
     private String Email;
     private String Password;
     private String Name;
     private String Type;
     public Register_input(String Email, String Password, String Name,String type) {
         this.Email = Email;
         this.Password = Password;
         this.Name = Name;
         this.Type = type;

     }

     public void setEmail(String email) {
         Email = email;
     }

     public String getEmail() {
         return Email;
     }

     public void setPassword(String password) {
         Password = password;
     }

     public String getPassword() {
         return Password;
     }

     public void setName(String name) {
         Name = name;
     }

     public String getName() {
         return Name;
     }

     public void setType(String type) {
         Type = type;
     }

     public String getType() {
         return Type;
     }
 }

    public class Register_output{

        private String return_message;


        public void setReturn_message(String return_message) {
            this.return_message = return_message;
        }

        public String getReturn_message() {
            return return_message;
        }
    }
}
