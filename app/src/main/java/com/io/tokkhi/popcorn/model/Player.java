package com.io.tokkhi.popcorn.model;

import com.io.tokkhi.popcorn.model.SingleItemModel;

import java.util.ArrayList;

/**
 * Created by Tokkhi on 2/14/2018.
 */

public class Player {
    private String name;
    private String club;
    private int type;
    private ArrayList<SingleItemModel> list1;

    public Player(String name, String club) {
        this.name = name;
        this.club = club;
    }
    public Player(String name, String club,int type) {
        this.name = name;
        this.club = club;
        this.type = type;

    }

    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }

    public String getClub() {
        return club;
    }

    public void setClub(String club) {
        this.club = club;
    }
    public int getType() {
        return type;
    }
    public void setType(int type) {
        this.type = type;
    }


}


