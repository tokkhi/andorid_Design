package com.io.tokkhi.popcorn.Adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.io.tokkhi.popcorn.R;

public class SeaechAdapter extends CursorAdapter {

    private final LayoutInflater cursorInflater;

    public SeaechAdapter(Context context, Cursor c, SearchView sv) {
        super(context, c,false);
        cursorInflater = (LayoutInflater) context.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);
    }

    public void bindView(View view, Context context, Cursor cursor) {
        TextView textViewTitle = (TextView) view.findViewById(R.id.name);
        String title = cursor.getString( cursor.getColumnIndex("name") );
        textViewTitle.setText(title);
    }

    public View newView(Context context, Cursor cursor, ViewGroup parent) {

        return cursorInflater.inflate(R.layout.list_search, parent, false);
    }
}
