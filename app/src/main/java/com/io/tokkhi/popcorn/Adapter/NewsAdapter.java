package com.io.tokkhi.popcorn.Adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.io.tokkhi.popcorn.R;
import com.io.tokkhi.popcorn.model.News;
import com.io.tokkhi.popcorn.model.Player;
import com.io.tokkhi.popcorn.webviewActivity;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tokkhi on 2/15/2018.
 */

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.ViewHolder> {

private ArrayList<News.item> mPlayers;
private Context mContext;
private WebView myWebView;
    ProgressDialog mProgressDialog;

public static class ViewHolder extends RecyclerView.ViewHolder {
    public TextView mName;
    public ImageView mClub;
    private final View view;

    public ViewHolder(View view) {
        super(view);
        mName = (TextView) view.findViewById(R.id.name);
        mClub = (ImageView) view.findViewById(R.id.imagetitle);
        this.view = view;
    }
}

    public NewsAdapter(Context context, ArrayList<News.item> dataset) {
        mPlayers = dataset;
        mContext = context;
    }

    @Override
    public NewsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.item_news, parent, false);

        NewsAdapter.ViewHolder viewHolder = new NewsAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(NewsAdapter.ViewHolder viewHolder, int position) {
        final News.item player = mPlayers.get(position);
        viewHolder.mName.setText(player.getName());
        new DownLoadImageTask(viewHolder.mClub).execute(player.getUrl());
        viewHolder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), webviewActivity.class);
                intent.putExtra("url",player.getUrl());
                v.getContext().startActivity(intent);
                Toast.makeText(v.getContext(), player.getName(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return mPlayers.size();
    }

    private class Title extends AsyncTask<String, Void, Void> {
        TextView tv;
        String title;
        public Title(TextView mName) {
            this.tv = mName;
        }

        @Override
        protected Void doInBackground(String... strings) {
            try {
                // Connect to the web site
                Document document = Jsoup.connect(strings[0]).get();
                // Get the html document title
                title = document.title();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(Void result) {
            tv.setText(title);
        }
    }


    private static class DownLoadImageTask extends AsyncTask<String,Void,Bitmap> {
        ImageView imageView;
    public DownLoadImageTask(ImageView  imageView) {
            this.imageView = imageView;
        }

        @Override
        protected Bitmap doInBackground(String... strings) {
            String urlOfImage = strings[0];
            Bitmap logo = null;
            try{

                // Connect to the web site
                Document document = Jsoup.connect(urlOfImage).get();
                // Using Elements to get the class data
                Elements img = document.select("div[class=main-pic] img[src]");
                // Locate the src attribute
                String imgSrc = img.attr("src");
                // Download image from URL

                InputStream input = new java.net.URL(imgSrc).openStream();
                // Decode Bitmap

                //logo.createScaledBitmap(logo,500,500,false);
                logo = BitmapFactory.decodeStream(input);

            }catch(Exception e){ // Catch the download exception
                e.printStackTrace();
            }
            return logo;
        }

        protected void onPostExecute(Bitmap result){
            imageView.setImageBitmap(result);
        }
    }

}