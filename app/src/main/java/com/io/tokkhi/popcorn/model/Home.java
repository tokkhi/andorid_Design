package com.io.tokkhi.popcorn.model;

import java.util.ArrayList;

/**
 * Created by Tokkhi on 4/19/2018.
 */

public class Home {

    private item_Movie Top_Movie;
    private ArrayList<item_Movie>Top5_Movie;
    private ArrayList<item_News>New_News;
    private ArrayList<item_Movie>Commingsoon;

    public ArrayList<item_Movie> getTop5_Movie() {
        return Top5_Movie;
    }

    public void setTop5_Movie(ArrayList<item_Movie> top5_Movie) {
        Top5_Movie = top5_Movie;
    }

    public item_Movie getTop_movie() {
        return Top_Movie;
    }

    public void setTop_movie(item_Movie Top_Movie) {
        this.Top_Movie = Top_Movie;
    }

    public ArrayList<item_News> getNew_News() {
        return New_News;
    }

    public void setNew_News(ArrayList<item_News> new_News) {
        New_News = new_News;
    }

    public ArrayList<item_Movie> getCommingsoon() {
        return Commingsoon;
    }

    public void setCommingsoon(ArrayList<item_Movie> commingsoon) {
        Commingsoon = commingsoon;
    }

    public class item_News{
        private String Name;
        private String ID;
        private String Url;


        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }

        public String getID() {
            return ID;
        }

        public void setID(String ID) {
            this.ID = ID;
        }

        public String getUrl() {
            return Url;
        }

        public void setUrl(String url) {
            Url = url;
        }
    }

    public class item_Movie{
        private String ID_Movie;
        private String Name_Movie;
        private String img;
        private Float Score_Users;
        private Float Score_Master;
        private String Date;

        public String getName_Movie() {
            return Name_Movie;
        }

        public void setName_Movie(String name_Movie) {
            Name_Movie = name_Movie;
        }

        public String getID_Movie() {
            return ID_Movie;
        }

        public void setID_Movie(String ID_Movie) {
            this.ID_Movie = ID_Movie;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public Float getScore_Users() {
            return Score_Users;
        }

        public void setScore_Users(Float score_Users) {
            Score_Users = score_Users;
        }

        public Float getScore_Master() {
            return Score_Master;
        }

        public void setScore_Master(Float score_Master) {
            Score_Master = score_Master;
        }

        public String getDate() {
            return Date;
        }

        public void setDate(String date) {
            Date = date;
        }
    }
}
