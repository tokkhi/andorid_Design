package com.io.tokkhi.popcorn;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.io.tokkhi.popcorn.API.CallServer;
import com.io.tokkhi.popcorn.API.api_link;
import com.io.tokkhi.popcorn.Adapter.CommentAdapter;
import com.io.tokkhi.popcorn.Adapter.SectionListDataAdapter;
import com.io.tokkhi.popcorn.model.Caster;
import com.io.tokkhi.popcorn.model.Director;
import com.io.tokkhi.popcorn.model.Login_input;
import com.io.tokkhi.popcorn.model.Login_output;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;

import Helper.Add_list;
import Helper.DownLoadImageTask;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Detail_cast_dir extends AppCompatActivity {
    private ArrayList<Caster.item> dataCaster;
    private ArrayList<Director.item> dataDirector;
    private RoundedImageView img;
    private TextView tv_name;
    private TextView tv_date;
    private TextView tv_detail;
    private RecyclerView rv;
    private ProgressBar pb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_cast_dir);
        Intent intent = getIntent();
        String id = intent.getStringExtra("id");
        String tag = intent.getStringExtra("tag");

        if(tag.equals("Caster")){
            getCaster(id);
        }else if(tag.equals("Director")){
           getDirector(id);

        }


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        img = (RoundedImageView) findViewById(R.id.itemImage);
        tv_name = (TextView) findViewById(R.id.Name);
        tv_date = (TextView)findViewById(R.id.Date);
        tv_detail = (TextView) findViewById(R.id.Detail);
        rv = (RecyclerView) findViewById(R.id.recycler_view);
        pb = findViewById(R.id.progressBar1);


    }

    private void getDirector(String id) {
        api_link api = new CallServer().Call();
        Call<Director> call = api.Director_D(new Director.input(id));
        call.enqueue(new Callback<Director>() {

            @Override
            public void onResponse(Call<Director> call, Response<Director> response) {
                dataDirector = response.body().getDetail_Director();
                tv_name.setText(dataDirector.get(0).getName());
                tv_date.setText("วันเกิด : "+dataDirector.get(0).getBrithday());
                tv_detail.setText(dataDirector.get(0).getDetail());
                img.setVisibility(View.GONE);
                pb.setVisibility(View.VISIBLE);
                String url_img = getApplication().getResources().getString(R.string.url_img) + dataDirector.get(0).getImg();
                new DownLoadImageTask(img,pb).execute(url_img);
                if(dataDirector.get(0).getMovie() != null) {
                    ArrayList singleSectionItems = new Add_list().D_Movie(dataDirector.get(0).getMovie());
                    SectionListDataAdapter itemListDataAdapter = new SectionListDataAdapter(getApplicationContext(), singleSectionItems, "Movie");
                    rv.setHasFixedSize(true);
                    rv.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
                    rv.setAdapter(itemListDataAdapter);
                }
            }
            @Override
            public void onFailure(Call<Director> call, Throwable t) {

            }


        });
    }


    private void getCaster(String id) {
        api_link api = new CallServer().Call();
        Call<Caster> call = api.Caster_D(new Caster.input(id));
        call.enqueue(new Callback<Caster>() {

            @Override
            public void onResponse(Call<Caster> call, Response<Caster> response) {

                dataCaster = response.body().getDetail_Caster();
                tv_name.setText(dataCaster.get(0).getName());
                tv_date.setText(dataCaster.get(0).getBrithday());
                tv_detail.setText(dataCaster.get(0).getDetail());
                img.setVisibility(View.GONE);
                pb.setVisibility(View.VISIBLE);
                String url_img = getApplication().getResources().getString(R.string.url_img) + dataCaster.get(0).getImg();
                new DownLoadImageTask(img,pb).execute(url_img);
                if(dataCaster.get(0).getMovie() != null){
                    ArrayList singleSectionItems = new Add_list().D_Movie(dataCaster.get(0).getMovie());
                    SectionListDataAdapter itemListDataAdapter = new SectionListDataAdapter(getApplicationContext(), singleSectionItems,"Movie");
                    rv.setHasFixedSize(true);
                    rv.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
                    rv.setAdapter(itemListDataAdapter);
                }

            }
            @Override
            public void onFailure(Call<Caster> call, Throwable t) {

            }


        });
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
