package com.io.tokkhi.popcorn.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.io.tokkhi.popcorn.R;
import com.io.tokkhi.popcorn.model.Home;
import com.io.tokkhi.popcorn.model.SectionDataModel;
import com.io.tokkhi.popcorn.webviewActivity;
import com.makeramen.roundedimageview.RoundedImageView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.InputStream;
import java.util.ArrayList;

import Helper.Add_list;
import Helper.DownLoadImageTask;

/**
 * Created by Tokkhi on 2/14/2018.
 */

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.ViewHolder> {

    private ArrayList<Home> mvalue;
    private Context mContext;
    private Home value;


    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView tv_master;
        private final TextView tv_User;
        private final ImageView img_master;
        private final ImageView img_user;
        public TextView tvName;
        private TextView tvDate;

        public RoundedImageView imageMovie;

        public ViewHolder(View view) {
            super(view);

            tvName = (TextView) view.findViewById(R.id.Name);
            tvDate = (TextView) view.findViewById(R.id.Date);
            imageMovie = (RoundedImageView) view.findViewById(R.id.imageMovie);
            tv_master = (TextView) view.findViewById(R.id.tv_master);
            tv_User = (TextView) view.findViewById(R.id.tv_user);
            img_master = (ImageView) view.findViewById(R.id.img_master);
            img_user = (ImageView) view.findViewById(R.id.img_user);

        }
    } class ViewHolder1 extends ViewHolder  {
        private TextView mName;
        private TextView mMore;
        private TextView tvDate;
        private TextView tv_null;
        private RecyclerView mrecyclerView;
        public ViewHolder1(View view) {
            super(view);
            mrecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_list);
            mName = view.findViewById(R.id.Name);
            mMore = view.findViewById(R.id.More);
            tvDate = view.findViewById(R.id.Date);
            tv_null= view.findViewById(R.id.tv_null);

        }


    }
    public class ViewHolder2 extends ViewHolder implements View.OnClickListener {
        private final TextView mName;
        private final ImageView mImage;

        public ViewHolder2(View view) {
            super(view);
            mName = (TextView) view.findViewById(R.id.tvName);
            mImage = (ImageView) view.findViewById(R.id.itemImage);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(v.getContext(), webviewActivity.class);
            intent.putExtra("url",value.getNew_News().get(getAdapterPosition()-2).getUrl());
            v.getContext().startActivity(intent);
        }
    }

    public HomeAdapter(Context context, ArrayList<Home> dataset) {
        this.mvalue = dataset;
        this.mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view ;
        ViewHolder viewHolder;

        switch (viewType) {
            case 0:  return new HomeAdapter.ViewHolder1(view = LayoutInflater.from(mContext)
                    .inflate(R.layout.item_list_cast_movie, parent, false));
            case 1: {
                return new HomeAdapter.ViewHolder1(view = LayoutInflater.from(mContext)
                        .inflate(R.layout.item_list_cast_movie, parent, false));
            }
            default: return new HomeAdapter.ViewHolder2(view = LayoutInflater.from(mContext)
                    .inflate(R.layout.item_movie, parent, false));
        }

    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        value = mvalue.get(0);
        switch (viewHolder.getItemViewType()) {
            case 0: {

                HomeAdapter.ViewHolder1 Holder = (HomeAdapter.ViewHolder1)viewHolder;
                Top5Adapter itemListDataAdapter = new Top5Adapter(mContext, value.getTop5_Movie());
                Holder.mrecyclerView.setHasFixedSize(true);
                Holder.mrecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
                Holder.mrecyclerView.setAdapter(itemListDataAdapter);
                SnapHelper snapHelper = new PagerSnapHelper();
                snapHelper.attachToRecyclerView( Holder.mrecyclerView);
                Holder.mName.setText("หนังมาแรง");
                break;
            }
            case 1:{
                HomeAdapter.ViewHolder1 Holder = (HomeAdapter.ViewHolder1)viewHolder;
                Holder.mName.setText("หนังใหม่กำลังมา");
                if(value.getCommingsoon().size() == 0) {
                    Holder.tv_null.setVisibility(View.VISIBLE);
                    Holder.mrecyclerView.setVisibility(View.GONE);

                }else {
                    ArrayList singleSectionItems = new Add_list().Add_Top5(value.getCommingsoon());
                    SectionListDataAdapter itemListDataAdapter = new SectionListDataAdapter(mContext, singleSectionItems, "Movie");
                    Holder.mrecyclerView.setHasFixedSize(true);
                    Holder.mrecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
                    Holder.mrecyclerView.setAdapter(itemListDataAdapter);

                }
                break;
            }
            default:{
                HomeAdapter.ViewHolder2 Holder = (HomeAdapter.ViewHolder2)viewHolder;
                Holder.mName.setText(value.getNew_News().get(position-2).getName());
                new DownLoadImageTask(Holder.mImage).execute(value.getNew_News().get(position-2).getUrl());
                break;
            }

        }
    }

    @Override
    public int getItemCount() {
        if(mvalue.get(0).getNew_News().equals(null)){
            return 2;
        }
        return mvalue.get(0).getNew_News().size()+2;
    }

    @Override
    public int getItemViewType(int position) {
            return position;



    }
    private static class DownLoadImageTask extends AsyncTask<String,Void,Bitmap> {
        ImageView imageView;
        public DownLoadImageTask(ImageView  imageView) {
            this.imageView = imageView;
        }

        @Override
        protected Bitmap doInBackground(String... strings) {
            String urlOfImage = strings[0];
            Bitmap logo = null;
            try{

                // Connect to the web site
                Document document = Jsoup.connect(urlOfImage).get();
                // Using Elements to get the class data
                Elements img = document.select("div[class=main-pic] img[src]");
                // Locate the src attribute
                String imgSrc = img.attr("src");
                // Download image from URL

                InputStream input = new java.net.URL(imgSrc).openStream();
                // Decode Bitmap

                //logo.createScaledBitmap(logo,500,500,false);
                logo = BitmapFactory.decodeStream(input);

            }catch(Exception e){ // Catch the download exception
                e.printStackTrace();
            }
            return logo;
        }

        protected void onPostExecute(Bitmap result){
            imageView.setImageBitmap(result);
        }
    }


}