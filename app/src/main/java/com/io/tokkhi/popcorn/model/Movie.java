package com.io.tokkhi.popcorn.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tokkhi on 2/24/2018.
 */

public class  Movie {

    @SerializedName("List_Movie")
    @Expose
    private ArrayList<Item.movie_list> movie_list = new ArrayList<>();
    @SerializedName("Detail_Movie")
    @Expose
    private ArrayList<Item.movie_Detail> detail_movie = new ArrayList<>();
    @SerializedName("Comment")
    @Expose
    private ArrayList<Item.Comment>Comment;
    /**
     * @return The contacts
     */
    public ArrayList<Item.movie_list> getMovie_list() {
        return movie_list;
    }

    /**
     * @param movie_list The contacts
     */
    public void setMovie_list(ArrayList<Item.movie_list> movie_list) {
        this.movie_list = movie_list;
    }

    public ArrayList<Item.movie_Detail> getDetail_movie() {
        return detail_movie;
    }

    public void setDetail_movie(ArrayList<Item.movie_Detail> detail_movie) {
        this.detail_movie = detail_movie;
    }

    public ArrayList<Item.Comment> getComment() {
        return Comment;
    }

    public void setComment(ArrayList<Item.Comment> comment) {
        Comment = comment;
    }

    public static class Input{
        private String id;
        private String guid;


        public Input(String id,String Guid) {
            this.id = id;
            this.guid = Guid;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getGuid() {
            return guid;
        }

        public void setGuid(String guid) {
            guid = guid;
        }
    }
    public static class Input_Genre{
        private String input;

        public Input_Genre(String id) {
            this.input = id;
        }

        public String getInput() {
            return input;
        }

        public void setInput(String input) {
            this.input = input;
        }
    }
}
