package com.io.tokkhi.popcorn;
        import android.os.Bundle;
import android.support.v4.app.Fragment;
        import android.support.v7.widget.GridLayoutManager;
        import android.support.v7.widget.RecyclerView;
        import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

        import com.io.tokkhi.popcorn.API.CallServer;
        import com.io.tokkhi.popcorn.API.api_link;
        import com.io.tokkhi.popcorn.Adapter.MovieAdapter;
        import com.io.tokkhi.popcorn.model.Item;
        import com.io.tokkhi.popcorn.model.Movie;


        import java.util.ArrayList;

        import retrofit2.Call;
        import retrofit2.Callback;
        import retrofit2.Response;


public class movie extends Fragment{

    private ArrayList<Item.movie_list> contactList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.movie, container, false);
        final RecyclerView rv = v.findViewById(R.id.my_recycler_view);
        api_link api = new CallServer().Call();
        Call<Movie> call = api.Movie();
        contactList = new ArrayList<>();
        call.enqueue(new Callback<Movie>() {
            @Override
            public void onResponse(Call<Movie> call, Response<Movie> response) {
                contactList = response.body().getMovie_list();
                int numberOfColumns = 2;

                rv.setLayoutManager(new GridLayoutManager(getContext(),numberOfColumns));
               rv.setAdapter(new MovieAdapter(getContext(),contactList));
            }

            @Override
            public void onFailure(Call<Movie> call, Throwable t) {

            }
    });
        return v;
    }


}
