package com.io.tokkhi.popcorn.API;

/**
 * Created by Tokkhi on 2/24/2018.
 */
import com.io.tokkhi.popcorn.model.Caster;
import com.io.tokkhi.popcorn.model.Director;
import com.io.tokkhi.popcorn.model.Home;
import com.io.tokkhi.popcorn.model.Item;
import com.io.tokkhi.popcorn.model.Login_input;
import com.io.tokkhi.popcorn.model.Login_output;
import com.io.tokkhi.popcorn.model.Movie;
import com.io.tokkhi.popcorn.model.News;
import com.io.tokkhi.popcorn.model.Register;
import com.io.tokkhi.popcorn.model.Review;
import com.io.tokkhi.popcorn.model.Search;
import com.io.tokkhi.popcorn.model.Users;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
public interface api_link {

    @GET("Movie/Movie_List")
    Call<Movie> Movie();

    @POST("Login")
    Call<Login_output> login(@Body Login_input body);

    @POST("Register")
    Call<Register.Register_output> Register(@Body Register.Register_input body);

    @POST("Movie/Movie_Detail")
    Call<Movie> Movie_Detail(@Body Movie.Input body);

    @POST("Login/Users")
    Call<Users> User_Detail(@Body Users.Users_Input body);

    @POST("Caster/Caster_List")
    Call<Caster> Caster();

    @POST("Director/Director_List")
    Call<Director> Director();

    @POST("Search")
    Call<ArrayList<Search>> search();

    @POST("Caster/Caster_Detail")
    Call<Caster> Caster_D(@Body Caster.input body);

    @POST("Director/Director_Detail")
    Call<Director> Director_D(@Body Director.input body);

    @POST("News/News")
    Call<News> News();

    @POST("Review/score_User")
    Call<Review.output> Review_User(@Body Review.User body);

    @POST("Review/score_Master")
    Call<Review.output> Review_Master(@Body Review.Master body);

    @POST("Review/Comment")
    Call<Review.output> Review_Comment(@Body Review.Comment body);

    @POST("Home/Show")
    Call<Home> Home();

    @POST("Movie/Genre_Movie")
    Call<Movie> Movie_Genre(@Body Movie.Input_Genre body);

    @POST("Movie/Rateing_Movie")
    Call<Movie> Movie_Rate(@Body Movie.Input_Genre body);

    @POST("Review/Like")
    Call<Review.output> Comment_Like(@Body Review.Input_Like body);

    @POST("Login/Logout")
    Call<Review.output> Logout(@Body Users.Users_Input body);

    @POST("Review/Report_Comment")
    Call<Review.output> Report(@Body Review.Input_Report body);

    @POST("Review/Delete_Comment")
    Call<Review.output> Delete_Comment(@Body Review.Input_Delete body);

    @POST("Review/Edit_Comment")
    Call<Review.output> Edit_Comment(@Body Review.Input_Comment_Edit body);

    @POST("Register/Edit_M")
    Call<Review.output> Edit_User(@Body Users.Edit_Input body);

    @POST("Register/Edit_Password")
    Call<Review.output> Edit_Pass(@Body Users.Edit_Pass body);

    @POST("Login/Comment_User")
    Call<Users> Comment_User(@Body Users.Users_Input body);
}
