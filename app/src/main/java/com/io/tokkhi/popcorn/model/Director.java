package com.io.tokkhi.popcorn.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Tokkhi on 4/9/2018.
 */

public class Director {
    @SerializedName("List_Director")
    @Expose
    private ArrayList<Director.item_list> Director = new ArrayList<>();
    @SerializedName("Detail_Director")
    @Expose
    private ArrayList<Director.item> Detail_Director = new ArrayList<>();
    public ArrayList<Director.item_list> getDirector() {
        return Director;
    }

    public void setDirector(ArrayList<Director.item_list> director) {
        Director = director;
    }

    public ArrayList<Director.item> getDetail_Director() {
        return Detail_Director;
    }

    public void setDetail_Director(ArrayList<Director.item> detail_Director) {
        Detail_Director = detail_Director;
    }


    public class item_list{
        @SerializedName("ID")
        @Expose
        private String ID;
        @SerializedName("Name")
        @Expose
        private String Name;
        @SerializedName("img")
        @Expose
        private String img;

        public String getId() {
            return ID;
        }

        public void setId(String id) {
            this.ID = id;
        }

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            this.Name = name;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }



    }
    public static class input{
        private String ID;
        public input(String id){
            this.ID = id;
        }

        public String getID() {
            return ID;
        }

        public void setID(String ID) {
            this.ID = ID;
        }
    }
    public class item {
        private String Name;
        private String ID;
        private String img;
        private String Brithday;
        private String Detail;
        private ArrayList<Director.item.movie> Movie;


        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }

        public String getID() {
            return ID;
        }

        public void setID(String ID) {
            this.ID = ID;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public String getDetail() {
            return Detail;
        }

        public void setDetail(String detail) {
            Detail = detail;
        }

        public String getBrithday() {
            return Brithday;
        }

        public void setBrithday(String brithday) {
            Brithday = brithday;
        }

        public ArrayList<Director.item.movie> getMovie() {
            return Movie;
        }

        public void setMovie(ArrayList<Director.item.movie> movie) {
            Movie = movie;
        }

        public class movie {
            @SerializedName("ID_Movie")
            @Expose
            private String ID;
            @SerializedName("Name_Movie")
            @Expose
            private String Name;
            @SerializedName("img")
            @Expose
            private String img;

            public String getId() {
                return ID;
            }

            public void setId(String id) {
                this.ID = id;
            }

            public String getName() {
                return Name;
            }

            public void setName(String name) {
                this.Name = name;
            }

            public String getImg() {
                return img;
            }

            public void setImg(String img) {
                this.img = img;
            }
        }
    }
}
