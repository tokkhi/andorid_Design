package com.io.tokkhi.popcorn.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Tokkhi on 4/9/2018.
 */

public class Caster {
    @SerializedName("List_Caster")
    @Expose
    private ArrayList<item_list> Caster = new ArrayList<>();

    @SerializedName("Detail_Caster")
    @Expose
    private ArrayList<item> Detail_Caster = new ArrayList<>();

    public ArrayList<item_list> getCaster() {
        return Caster;
    }

    public void setCaster(ArrayList<item_list> caster) {
        Caster = caster;
    }

    public ArrayList<item> getDetail_Caster() {
        return Detail_Caster;
    }

    public void setDetail_Caster(ArrayList<item> detail_Caster) {
        Detail_Caster = detail_Caster;
    }

    public class item_list{
        @SerializedName("ID")
        @Expose
        private String ID;
        @SerializedName("Name")
        @Expose
        private String Name;
        @SerializedName("img")
        @Expose
        private String img;

        public String getId() {
            return ID;
        }

        public void setId(String id) {
            this.ID = id;
        }

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            this.Name = name;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }



    }
    public static class input{
        private String ID;
        public input(String id){
            this.ID = id;
        }

        public String getID() {
            return ID;
        }

        public void setID(String ID) {
            this.ID = ID;
        }
    }
    public class item{
        private String Name;
        private String ID;
        private String img;
        private String Brithday;
        private String Detail;
        private ArrayList<Director.item.movie> Movie;



        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }

        public String getID() {
            return ID;
        }

        public void setID(String ID) {
            this.ID = ID;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public String getDetail() {
            return Detail;
        }

        public void setDetail(String detail) {
            Detail = detail;
        }

        public String getBrithday() {
            return Brithday;
        }

        public void setBrithday(String brithday) {
            Brithday = brithday;
        }

        public ArrayList<Director.item.movie> getMovie() {
            return Movie;
        }

        public void setMovie(ArrayList<Director.item.movie> movie) {
            Movie = movie;
        }

        public class movie{
            @SerializedName("ID_Movie")
            @Expose
            private String ID;
            @SerializedName("Name_Movie")
            @Expose
            private String Name;
            @SerializedName("img")
            @Expose
            private String img;

            public String getId() {
                return ID;
            }

            public void setId(String id) {
                this.ID = id;
            }

            public String getName() {
                return Name;
            }

            public void setName(String name) {
                this.Name = name;
            }

            public String getImg() {
                return img;
            }

            public void setImg(String img) {
                this.img = img;
            }
        }

    }
}

