package com.io.tokkhi.popcorn;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.io.tokkhi.popcorn.API.CallServer;
import com.io.tokkhi.popcorn.API.api_link;
import com.io.tokkhi.popcorn.model.Login_input;
import com.io.tokkhi.popcorn.model.Login_output;
import com.io.tokkhi.popcorn.model.Users;
import com.google.firebase.iid.FirebaseInstanceId;

import android.preference.PreferenceManager;

import java.io.Serializable;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends AppCompatActivity {
    private Button login;
    private EditText email;
    private EditText pass;
    private Button regis;
    private String TAG;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        login = (Button) findViewById(R.id.bt_login);
        regis = (Button)findViewById(R.id.bt_regis);
        email = findViewById(R.id.et_Email);
        pass = findViewById(R.id.ed_password);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Bundle bundle = new Bundle();
        regis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(Login.this, Register.class);
                // myIntent.putExtra("key", value);
                Login.this.startActivity(myIntent);
            }
        });

        final String token = FirebaseInstanceId.getInstance().getToken();
        Log.d("Token:", token);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                api_link api = new CallServer().Call();
                Call<Login_output> call = api.login(new Login_input(email.getText().toString(),pass.getText().toString(),token));
                call.enqueue(new Callback<Login_output>() {


                    @Override
                    public void onResponse(Call<Login_output> call, Response<Login_output> response) {
                        String msg = response.body().getMessage();

                        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        SharedPreferences.Editor editor = settings.edit();

                        if(msg.equals("Success")){
                            String Guid = response.body().getToken().getGuid();
                            String Name = response.body().getName();
                            String img = response.body().getImg();
                            String type = response.body().getType();
                            editor.putString("Session", msg);
                            editor.putString("password", pass.getText().toString());
                            editor.putString("type", type);
                            editor.putString("Guid", Guid);
                            editor.putString("Name", Name);
                            editor.putString("img", img);
                            editor.commit();
                            Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
                            // myIntent.putExtra("key", value);
                            Login.this.startActivity(myIntent);
                        }else{
                            Toast.makeText(getApplicationContext(), "Login Fall!",
                                    Toast.LENGTH_LONG).show();
                        }

                    }
                    @Override
                    public void onFailure(Call<Login_output> call, Throwable t) {

                    }


                });

            }
        });
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
