package com.io.tokkhi.popcorn.model;

/**
 * Created by Tokkhi on 2/25/2018.
 */

public class Login_output {
    private Boolean error = true;
    private String Name;
    private String img;
    private String Type;
    private String return_message;
    private Boolean active;
    private Token token;

    /**
     *
     * @return
     * The error
     */
    public Boolean getError() {
        return error;
    }

    /**
     *
     * @param error
     * The error
     */
    public void setError(Boolean error) {
        this.error = error;
    }

    /**
     *
     * @return
     * The message
     */
    public String getName() {
        return Name;
    }



    /**
     *
     * @param Name
     * The message
     */
    public void setName(String Name) {
        this.Name = Name;
    }


    /**
     *
     * @return
     * The active
     */
    public Boolean getActive() {
        return active;
    }

    /**
     *
     * @param active
     * The active
     */
    public void setActive(Boolean active) {
        this.active = active;
    }


    public void setReturn_message(String return_message) {
        this.return_message = return_message;
    }

    public String getMessage() {
        return return_message;
    }

    public Token getToken() {
        return token;
    }

    public void setToken(Token token) {
        this.token = token;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public class Token{
        private String app_id;
        private String Guid;
        private String exp;

        public String getApp_id() {
            return app_id;
        }

        public void setApp_id(String app_id) {
            this.app_id = app_id;
        }

        public String getExp() {
            return exp;
        }

        public void setExp(String exp) {
            this.exp = exp;
        }

        public String getGuid() {
            return Guid;
        }

        public void setGuid(String guid) {
            Guid = guid;
        }
    }
}
