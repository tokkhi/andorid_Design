package com.io.tokkhi.popcorn.Adapter;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.io.tokkhi.popcorn.API.CallServer;
import com.io.tokkhi.popcorn.API.api_link;
import com.io.tokkhi.popcorn.Comment_More;
import com.io.tokkhi.popcorn.GenreActivity;
import com.io.tokkhi.popcorn.R;
import com.io.tokkhi.popcorn.Video_tri;
import com.io.tokkhi.popcorn.comment;
import com.io.tokkhi.popcorn.detail_move;
import com.io.tokkhi.popcorn.model.Item;
import com.io.tokkhi.popcorn.model.Review;
import com.pchmn.materialchips.ChipView;
import com.pierfrancescosoffritti.youtubeplayer.player.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayer;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayerInitListener;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayerView;

import java.util.ArrayList;

import Helper.Add_list;
import Helper.DownLoadImageTask;
import Helper.makeTextViewResizable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Tokkhi on 2/21/2018.
 */

public class Movie_DetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<Item.movie_Detail> mPlayers;
    private Context mContext;
    private android.view.View view;
    private Item.movie_Detail value;
    private String Guid;


    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final ImageView img_master;
        private final ImageView img_user;
        private final TextView tvmaster;
        private final TextView tvuser;
        private ImageView image;
        private TextView detail;
        private ImageButton video;
        private TextView title;
        public ProgressBar pb;


        public ViewHolder(View view) {
            super(view);
            tvmaster = (TextView) view.findViewById(R.id.tv_master);
            tvuser = (TextView) view.findViewById(R.id.tv_user);
            img_user = (ImageView)  view.findViewById(R.id.image_user);
            img_master = (ImageView)  view.findViewById(R.id.img_master);
            title = (TextView) view.findViewById(R.id.Title_Movie);
            detail = (TextView) view.findViewById(R.id.text_detail);
            image = view.findViewById(R.id.imageMovie);
            video = view.findViewById(R.id.video);
            pb = view.findViewById(R.id.progressBar1);


        }
    }
    class ViewHolder1 extends RecyclerView.ViewHolder {
        public TextView mName;
        public TextView mMore;
        public RecyclerView mrecyclerView;
        public Button bt_com;
        public TextView tv_null;

        public ViewHolder1(View view) {
            super(view);
            mrecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_list);
            mName = view.findViewById(R.id.Name);
            mMore = view.findViewById(R.id.More);
            bt_com = view.findViewById(R.id.bt_com);
            tv_null = view.findViewById(R.id.tv_null);
        }

    }
    class ViewHolder2 extends RecyclerView.ViewHolder {
        public TextView mstudio;
        public TextView mrate;
        public TextView mdate;
        public LinearLayout ll;
        public ViewHolder2(View view) {
            super(view);
            mrate = view.findViewById(R.id.Rate);
            mstudio = view.findViewById(R.id.studio);
            ll = view.findViewById(R.id.ll_genre);
            mdate = view.findViewById(R.id.date);
        }

    }
    class ViewHolder3 extends RecyclerView.ViewHolder {
        public EditText et_com;
        public Button bt_com;
        public ViewHolder3(View view) {
            super(view);
            et_com = view.findViewById(R.id.et_com);

            bt_com = view.findViewById(R.id.bt_com);
        }

    }

    public Movie_DetailAdapter(Context context, ArrayList<Item.movie_Detail> dataset) {
        mPlayers = dataset;
        mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view ;

        switch (viewType) {
            case 0:  return new ViewHolder(view = LayoutInflater.from(mContext)
                    .inflate(R.layout.item_title_movie, parent, false));
            case 1: {
                return new ViewHolder1(view = LayoutInflater.from(mContext)
                        .inflate(R.layout.item_list_cast_movie, parent, false));
            }
            case 2: {
                return new ViewHolder1(view = LayoutInflater.from(mContext)
                        .inflate(R.layout.item_list_cast_movie, parent, false));
            }
            case 3: {
                return new ViewHolder2(view = LayoutInflater.from(mContext)
                        .inflate(R.layout.item_detail_movie, parent, false));
            }
            case 4: {
                return new ViewHolder1(view = LayoutInflater.from(mContext)
                        .inflate(R.layout.movie_comment, parent, false));
            }
            case 5: {
                return new ViewHolder1(view = LayoutInflater.from(mContext)
                        .inflate(R.layout.movie_comment, parent, false));
            }
            default: return new ViewHolder1(view = LayoutInflater.from(mContext)
                    .inflate(R.layout.item_movie, parent, false));


        }
    }



    @SuppressLint("ResourceType")
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
         value = mPlayers.get(0);
        SharedPreferences get = PreferenceManager.getDefaultSharedPreferences(mContext);

        Guid = get.getString("Guid",null);
        final String type = get.getString("type",null);
        switch (holder.getItemViewType()) {
            case 0: {
                final ViewHolder viewHolder = (ViewHolder)holder;

                if(value.getScore_Master() > 7){
                    viewHolder.img_master.setImageResource(R.drawable.ic_master_good);
                }else if(value.getScore_Master() > 3){
                    viewHolder.img_master.setImageResource(R.drawable.ic_master_fine);
                }else{
                    viewHolder.img_master.setImageResource(R.drawable.ic_master_bad);
                }
                if(value.getScore_User() > 4){
                    viewHolder.img_user.setImageResource(R.drawable.ic_user_good);
                }else if(value.getScore_User() > 2.5){
                    viewHolder.img_user.setImageResource(R.drawable.ic_user_find);
                }else{
                    viewHolder.img_user.setImageResource(R.drawable.ic_user_bad);
                }
                viewHolder.tvmaster.setText(String.valueOf(value.getScore_Master()));


                viewHolder.tvuser.setText(String.valueOf(value.getScore_User()));
                viewHolder.title.setText(value.getName());
                viewHolder.detail.setText(value.getDetail());
                new makeTextViewResizable(viewHolder.detail, 3, "View More", true);
                String url_img = mContext.getResources().getString(R.string.url_img) + value.getImg();
                viewHolder.image.setVisibility(View.GONE);
                viewHolder.pb.setVisibility(View.VISIBLE);
                new DownLoadImageTask(viewHolder.image,viewHolder.pb).execute(url_img);

               /* if(value.getTrialer() != null && !value.getTrialer().isEmpty()){
                    viewHolder.video.setVisibility(View.VISIBLE);
                    viewHolder.video.initialize(new YouTubePlayerInitListener() {
                        @Override
                        public void onInitSuccess(final YouTubePlayer initializedYouTubePlayer) {
                            initializedYouTubePlayer.addListener(new AbstractYouTubePlayerListener() {
                                @Override
                                public void onReady() {
                                    initializedYouTubePlayer.cueVideo(value.getTrialer(), 0);

                                }

                                @Override
                                public void onStateChange(int state) {
                                    super.onStateChange(state);

                                }

                                @Override
                                public void onError(int error) {
                                    super.onError(error);

                                }
                            });
                        }
                    }, false);

                }else {
                    viewHolder.video.setVisibility(View.GONE);
                }*/
                if(value.getTrialer() != null && !value.getTrialer().isEmpty()){

                    viewHolder.video.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(v.getContext(), Video_tri.class);
                            intent.putExtra("url", value.getTrialer());
                            intent.putExtra("id", value.getId());
                            v.getContext().startActivity(intent);
                        }
                    });

                }else {
                    viewHolder.video.setVisibility(View.GONE);
                }
                viewHolder.img_user.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (Guid == null) {
                            login(mContext.getResources().getString(R.string.not_login));
                            Toast.makeText(v.getContext(), R.string.not_login, Toast.LENGTH_SHORT).show();
                        } else {
                            if(type.equals("2")){
                                review_u();
                            }else {
                                login(mContext.getResources().getString(R.string.c_user));
                            }

                        }
                    }
                });
                viewHolder.img_master.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (Guid == null) {

                            login(mContext.getResources().getString(R.string.not_login));
                            Toast.makeText(v.getContext(), R.string.not_login, Toast.LENGTH_SHORT).show();
                        } else {
                            if(type.equals("3")){
                                review_m();
                            }else {
                                login(mContext.getResources().getString(R.string.c_user));
                            }
                        }
                    }

                });

                break;
            }
            case 1: {
                ViewHolder1 viewHolder = (ViewHolder1)holder;
                if(value.getCaster().get(0).getId() == null) {
                    viewHolder.tv_null.setVisibility(View.VISIBLE);
                    viewHolder.mrecyclerView.setVisibility(View.GONE);
                }else {
                    ArrayList singleSectionItems = new Add_list().Add_Caster(value.getCaster());
                    SectionListDataAdapter itemListDataAdapter = new SectionListDataAdapter(mContext, singleSectionItems, "Caster");
                    viewHolder.mrecyclerView.setHasFixedSize(true);
                    viewHolder.mrecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
                    viewHolder.mrecyclerView.setAdapter(itemListDataAdapter);
                    viewHolder.mName.setText(R.string.Caster);
                }
                break;
            }
            case 2:{
                ViewHolder1 viewHolder = (ViewHolder1)holder;
                if(value.getDirector().get(0).getId() == null){
                    viewHolder.tv_null.setVisibility(View.VISIBLE);
                    viewHolder.mrecyclerView.setVisibility(View.GONE);
                }else {
                    ArrayList singleSectionItems = new Add_list().Add_Directer(value.getDirector());
                    SectionListDataAdapter itemListDataAdapter = new SectionListDataAdapter(mContext, singleSectionItems,"Director");
                    viewHolder.mrecyclerView.setHasFixedSize(true);
                    viewHolder.mrecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
                    viewHolder.mrecyclerView.setAdapter(itemListDataAdapter);
                }

                viewHolder.mName.setText(R.string.Directer);
                break;
            }
            case 3: {
                ViewHolder2 viewHolder = (ViewHolder2)holder;
                viewHolder.mstudio.setText(value.getStudio());
                viewHolder.mrate.setText(value.getRate());
                viewHolder.mdate.setText(value.getDate_Movie());
                if(value.getGenre().get(0).getName() != null){
                    for (final Item.Genre genre : value.getGenre()
                            ) {

                        ChipView c = new ChipView(mContext);
                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams( LinearLayout.LayoutParams.WRAP_CONTENT, 100);
                        params.setMargins(8,8,8,8);
                        c.setLayoutParams(params);
                        c.setLabel(genre.getName());
                        c.setLabelColor(Color.parseColor("#000000"));
                        c.setChipBackgroundColor(Color.parseColor("#ffff00"));
                        c.setOnChipClicked(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent = new Intent(mContext, GenreActivity.class);
                                intent.putExtra("id",genre.getId());
                                mContext.startActivity(intent);

                            }
                        });
                        viewHolder.ll.addView(c);

                    }
                }


                break;
            }
            //master
            case 4: {
                ViewHolder1 viewHolder = (ViewHolder1)holder;
                viewHolder.mName.setText(R.string.master);
                if(value.getComment_Master().size() == 0){

                }else {
                    CommentAdapter itemListDataAdapter = new CommentAdapter(mContext, value.getComment_Master(), 5);
                    viewHolder.mrecyclerView.setHasFixedSize(true);
                    viewHolder.mrecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
                    viewHolder.mrecyclerView.setAdapter(itemListDataAdapter);
                    viewHolder.mName.setText(R.string.master);
                    viewHolder.mMore.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(v.getContext(), Comment_More.class);
                            intent.putExtra("id", value.getId());
                            intent.putExtra("status", R.string.master);
                            intent.putExtra("item", value.getComment_Master());
                            v.getContext().startActivity(intent);
                        }
                    });

                }
                if(value.getCheck_Comment() != null && !value.getCheck_Comment()) {
                    if (type != null && type.equals("3")) {
                        viewHolder.bt_com.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (Guid == null) {
                                    login(mContext.getResources().getString(R.string.not_login));
                                    Toast.makeText(v.getContext(), R.string.not_login, Toast.LENGTH_SHORT).show();
                                } else {
                                    Intent intent = new Intent(v.getContext(), comment.class);
                                    intent.putExtra("id", value.getId());
                                    intent.putExtra("status", R.string.master);

                                    v.getContext().startActivity(intent);
                                }

                            }
                        });
                    } else {
                        viewHolder.bt_com.setVisibility(View.GONE);
                    }
                }else {
                    viewHolder.bt_com.setVisibility(View.GONE);
                }


                break;
            }
            //users
            case 5: {
                ViewHolder1 viewHolder = (ViewHolder1)holder;
                viewHolder.mName.setText(R.string.users);
                if(value.getComment_User().size() ==0){

                }else {
                    CommentAdapter itemListDataAdapter = new CommentAdapter(mContext, value.getComment_User(),5);
                    viewHolder.mrecyclerView.setHasFixedSize(true);
                    viewHolder.mrecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
                    viewHolder.mrecyclerView.setAdapter(itemListDataAdapter);
                    viewHolder.mMore.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(v.getContext(), Comment_More.class);
                            intent.putExtra("id", value.getId());
                            intent.putExtra("status", R.string.users);
                            intent.putExtra("item", value.getComment_User());
                            v.getContext().startActivity(intent);

                        }
                    });




                }
                if(value.getCheck_Comment() != null && !value.getCheck_Comment()) {
                    if (type != null && type.equals("2")) {
                        viewHolder.bt_com.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (Guid == null) {
                                    login(mContext.getResources().getString(R.string.not_login));
                                    Toast.makeText(v.getContext(), R.string.not_login, Toast.LENGTH_SHORT).show();
                                } else {
                                    Intent intent = new Intent(v.getContext(), comment.class);
                                    intent.putExtra("id", value.getId());
                                    v.getContext().startActivity(intent);
                                }

                            }
                        });
                    } else {
                        viewHolder.bt_com.setVisibility(View.GONE);
                    }

                }else {
                    viewHolder.bt_com.setVisibility(View.GONE);
                }
                break;
            }


            default:{
                ViewHolder1 viewHolder = (ViewHolder1)holder;
                viewHolder.mName.setText(value.getName());

                break;
            }

        }
    }

    private void review_m() {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.popup_master);
        dialog.setCancelable(true);
        Button bt_review = (Button) dialog.findViewById(R.id.bt_review);
        final RatingBar rate = (RatingBar) dialog.findViewById(R.id.ratingBar);
        bt_review.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                api_link api = new CallServer().Call();
                Call<Review.output> call = api.Review_Master(new Review.Master(Guid, rate.getRating(), value.getId()));
                call.enqueue(new Callback<Review.output>() {
                    public Movie_DetailAdapter adapter;

                    @Override
                    public void onResponse(Call<Review.output> call, Response<Review.output> response) {
                        dialog.cancel();
                        Intent intent = new Intent(mContext.getApplicationContext(), detail_move.class);
                        intent.putExtra("id", value.getId());
                        mContext.getApplicationContext().startActivity(intent);
                    }

                    @Override
                    public void onFailure(Call<Review.output> call, Throwable t) {

                    }
                });


            }
        });
        dialog.show();
    }

    private void review_u() {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.popup);
        dialog.setCancelable(true);
        Button bt_review = (Button) dialog.findViewById(R.id.bt_review);
        final RatingBar rate = (RatingBar) dialog.findViewById(R.id.ratingBar);
        bt_review.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                api_link api = new CallServer().Call();
                Call<Review.output> call = api.Review_User(new Review.User(Guid, rate.getRating(), value.getId()));
                call.enqueue(new Callback<Review.output>() {


                    @Override
                    public void onResponse(Call<Review.output> call, Response<Review.output> response) {
                        dialog.cancel();
                        Intent intent = new Intent(mContext.getApplicationContext(), detail_move.class);
                        intent.putExtra("id", value.getId());
                        mContext.getApplicationContext().startActivity(intent);


                    }

                    @Override
                    public void onFailure(Call<Review.output> call, Throwable t) {

                    }
                });
            }

        });
        dialog.show();
    }

    private void login(String text) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.popup_nonlogin);
        dialog.setCancelable(true);

        TextView textView1 = (TextView)dialog.findViewById(R.id.tv_s);
        Button btc = dialog.findViewById(R.id.bt_close);
        textView1.setText(text);

        btc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        dialog.show();


    }


    @Override
    public int getItemViewType(int position) {

            return position;
        }

    @Override
    public int getItemCount() {
        return 6;
    }

}

