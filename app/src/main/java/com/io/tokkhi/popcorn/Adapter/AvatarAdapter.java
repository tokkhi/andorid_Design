package com.io.tokkhi.popcorn.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ahmadrosid.svgloader.SvgLoader;
import com.io.tokkhi.popcorn.Detail_cast_dir;
import com.io.tokkhi.popcorn.Edit_UserActivity;
import com.io.tokkhi.popcorn.R;
import com.io.tokkhi.popcorn.model.Caster;

import java.util.ArrayList;

import Helper.DownLoadImageTask;

public class AvatarAdapter extends RecyclerView.Adapter<AvatarAdapter.ViewHolder> {

private static ArrayList<String> mData = null;
private Context mContext;
private Drawable item;

public static class ViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener {

    public ImageView mImage;


    public ViewHolder(View view) {
        super(view);
        mImage = (ImageView) view.findViewById(R.id.icon);
        view.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(v.getContext());
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("img", mData.get(getAdapterPosition()));
        editor.commit();
        Intent intent = new Intent(v.getContext(), Edit_UserActivity.class);
        v.getContext().startActivity(intent);
    }
}

    public AvatarAdapter(Context context, ArrayList<String> Data) {
        mData = Data;
        mContext = context;
    }


    @Override
    public AvatarAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.item_avatar, parent, false);
        return  new AvatarAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(AvatarAdapter.ViewHolder viewHolder, int position) {

        String url = mContext.getResources().getString(R.string.Url_svg) + mData.get(position);



        SvgLoader.pluck()
                .with((Activity) mContext)
                .setPlaceHolder(R.mipmap.ic_launcher, R.mipmap.ic_launcher)
                .load(url, viewHolder.mImage);

    }


    @Override
    public int getItemCount() {
        return mData.size();
    }



}