package com.io.tokkhi.popcorn.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ahmadrosid.svgloader.SvgLoader;
import com.io.tokkhi.popcorn.API.CallServer;
import com.io.tokkhi.popcorn.API.api_link;
import com.io.tokkhi.popcorn.Comment_More;
import com.io.tokkhi.popcorn.Edit_CommentActivity;
import com.io.tokkhi.popcorn.R;
import com.io.tokkhi.popcorn.comment;
import com.io.tokkhi.popcorn.detail_move;
import com.io.tokkhi.popcorn.model.Item;
import com.io.tokkhi.popcorn.model.Review;

import java.util.ArrayList;

import Helper.DownLoadImageTask;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Tokkhi on 3/24/2018.
 */

public class CommentAdapter   extends RecyclerView.Adapter<CommentAdapter.Holder> {

    private final ArrayList<Item.Comment> itemsList;
    private Context mContext;
    private LayoutInflater mInflater;
    private Item.Comment item;
    private int size;
    private int position;

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }



        public class Holder extends RecyclerView.ViewHolder  implements View.OnCreateContextMenuListener {

            protected TextView tvTitle;
            protected TextView date;
            protected TextView tvcom;
            protected TextView tv_like;
            protected ImageView itemImage;
            protected ImageView iv_like;



            public Holder(View view) {
                super(view);

                this.tvTitle = (TextView) view.findViewById(R.id.name);
                this.itemImage = (ImageView) view.findViewById(R.id.iv_user);
                this.date = view.findViewById(R.id.date);
                this.tvcom = view.findViewById(R.id.comment);
                this.tv_like = view.findViewById(R.id.tv_like);
                this.iv_like = view.findViewById(R.id.iv_like);
                view.setOnCreateContextMenuListener(this);

            }



            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                SharedPreferences get = PreferenceManager.getDefaultSharedPreferences(mContext);
                final String Guid = get.getString("Guid",null);
                String id_u = itemsList.get(getAdapterPosition()).getId_u();

                if(Guid != null){
                    if (Guid.equals(id_u)) {
                        menu.add("ลบ").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                Toast.makeText(mContext, "ลบ"+itemsList.get(getAdapterPosition()).getId_u() , Toast.LENGTH_SHORT).show();
                                api_link api = new CallServer().Call();
                                Call<Review.output> call = api.Delete_Comment(new Review.Input_Delete(itemsList.get(getAdapterPosition()).getId()));
                                call.enqueue(new Callback<Review.output>() {
                                    @Override
                                    public void onResponse(Call<Review.output> call, Response<Review.output> response) {
                                        Review.output output = response.body();
                                        if (output.getReturn_message().equals("Success")){
                                            removeAt(getAdapterPosition());
                                            Toast.makeText(mContext, R.string.Success, Toast.LENGTH_SHORT).show();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<Review.output> call, Throwable t) {

                                    }
                                });
                                return true;
                            }
                        });
                        menu.add("แก้ไข").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                Toast.makeText(mContext, "แก้ไข"+itemsList.get(getAdapterPosition()).getComment() , Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(mContext, Edit_CommentActivity.class);
                                intent.putExtra("id_m", itemsList.get(getAdapterPosition()).getId_m());
                                intent.putExtra("text",itemsList.get(getAdapterPosition()).getComment());
                                intent.putExtra("id_c", itemsList.get(getAdapterPosition()).getId());
                                itemView.getContext().startActivity(intent);
                                return true;
                            }
                        });

                } else {
                        menu.add("รายงาน").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                if (Guid == null) {

                                } else {
                                    api_link api = new CallServer().Call();
                                    Call<Review.output> call = api.Report(new Review.Input_Report(itemsList.get(getAdapterPosition()).getId(), Guid));
                                    call.enqueue(new Callback<Review.output>() {
                                        @Override
                                        public void onResponse(Call<Review.output> call, Response<Review.output> response) {
                                            Review.output output = response.body();
                                            if (output.getReturn_message().equals("Success")) {
                                                Toast.makeText(mContext, R.string.Success, Toast.LENGTH_SHORT).show();
                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<Review.output> call, Throwable t) {

                                        }
                                    });
                                }

                                return true;
                            }

                        });
                    }
                }


            }

        }

    public CommentAdapter( Context context, ArrayList<Item.Comment> itemsList, int size) {
        super();
        this.itemsList = itemsList;
        this.mContext = context;
        this.size = size;
    }




    @Override
    public CommentAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.item_comment_movie, parent, false);
        return  new CommentAdapter.Holder(itemView);
    }

    @Override
    public void onBindViewHolder(final Holder holder, int position) {
        final Item.Comment mItem = itemsList.get(position);

        SharedPreferences get = PreferenceManager.getDefaultSharedPreferences(mContext);
        final String Guid = get.getString("Guid",null);

        holder.tvcom.setText(mItem.getComment());
        holder.tvTitle.setText(mItem.getName());
        holder.tv_like.setText(mItem.getLike());

        holder.date.setText(mItem.getDate());
        if (mItem.getCheck()) {
            holder.iv_like.setImageResource(R.drawable.ic_favorite_black);
            holder.tv_like.setTextColor(Color.RED);
        } else {
            holder.iv_like.setImageResource(R.drawable.ic_favorite_border_black_24dp);
            holder.tv_like.setTextColor(Color.GRAY);
        }

        if(holder.itemImage != null){
            String url = mContext.getResources().getString(R.string.Url_svg) + mItem.getImg();
            SvgLoader.pluck()
                    .with((Activity) mContext)
                    .setPlaceHolder(R.mipmap.ic_launcher, R.mipmap.ic_launcher)
                    .load(url, holder.itemImage);
        }


        holder.iv_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Guid == null) {
                    login(mContext.getResources().getString(R.string.not_login));
                    Toast.makeText(mContext, R.string.not_login, Toast.LENGTH_SHORT).show();
                }else{
                    if (mItem.getCheck()){
                        //update unlike drawable
                        mItem.setCheck(false);
                        holder.iv_like.setImageResource(R.drawable.ic_favorite_border_black_24dp);
                        holder.tv_like.setTextColor(Color.GRAY);
                        mItem.setLike(String.valueOf(Integer.parseInt(mItem.getLike())-1));
                        holder.tv_like.setText(mItem.getLike());

                    } else {
                        //update like drawable
                        mItem.setCheck(true);
                        holder.iv_like.setImageResource(R.drawable.ic_favorite_black);
                        holder.tv_like.setTextColor(Color.RED);
                        mItem.setLike(String.valueOf(Integer.parseInt(mItem.getLike())+1));
                        holder.tv_like.setText(mItem.getLike());
                    }
                    updateLike(mItem.getId());
                }

            }
        });

       /* new DownLoadImageTask(holder.itemImage).execute(singleItem.getImg());*/
    }

    private void login(String text) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.popup_nonlogin);
        dialog.setCancelable(true);

        TextView textView1 = dialog.findViewById(R.id.tv_s);
        Button btc = dialog.findViewById(R.id.bt_close);
        textView1.setText(text);

        btc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        dialog.show();
    }

    private void updateLike(String id) {
        SharedPreferences get = PreferenceManager.getDefaultSharedPreferences(mContext);
        final String Guid = get.getString("Guid",null);
        api_link api = new CallServer().Call();
        Call<Review.output> call = api.Comment_Like(new Review.Input_Like(Guid,id));
        call.enqueue(new Callback<Review.output>() {


            @Override
            public void onResponse(Call<Review.output> call, Response<Review.output> response) {
                Review.output output = response.body();
                if (output.getReturn_message() == "Success"){
                    Toast.makeText(mContext, "Success", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<Review.output> call, Throwable t) {

            }
        });
        }

    @Override
    public int getItemCount() {
        if(size == 999 || itemsList.size() < size){
            return itemsList.size();
            }else{
            return size;
        }

    }
    public void removeAt(int position) {
        itemsList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, itemsList.size());
    }


}


