package com.io.tokkhi.popcorn;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ahmadrosid.svgloader.SvgLoader;
import com.io.tokkhi.popcorn.API.CallServer;
import com.io.tokkhi.popcorn.API.api_link;
import com.io.tokkhi.popcorn.model.Review;
import com.io.tokkhi.popcorn.model.Users;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class User extends AppCompatActivity {

    private TextView tvname;
    private Button bt_review;
    private Users item;
    private Button bt_add;
    private String Guid;
    private String img;
    private ImageView mImage;
    private TextView tv_status;
    private String type;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        SharedPreferences  get = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
         Guid = get.getString("Guid",null);
         img = get.getString("img",null);
        type = get.getString("type",null);
        getvalue(Guid);
        tvname = findViewById(R.id.name);
        Button bt_logout = findViewById(R.id.bt_logout);
        bt_add =  findViewById(R.id.bt_add);
        bt_review =  findViewById(R.id.bt_review);
        Button bt_Edit =  findViewById(R.id.bt_edit);
        mImage =  findViewById(R.id.imageView1);
        tv_status =  findViewById(R.id.status);
        switch (type){
            case "1":
                tv_status.setText("admin");
                break;
            case "2":
                tv_status.setText(getResources().getString(R.string.user));
                break;
            case "3":
                tv_status.setText(getResources().getString(R.string.master));
                break;
            default:
                tv_status.setText(getResources().getString(R.string.user));
                break;
        }
        if(img != null){
            String url = getResources().getString(R.string.Url_svg) + img;
            SvgLoader.pluck()
                    .with(this)
                    .setPlaceHolder(R.mipmap.ic_launcher, R.mipmap.ic_launcher)
                    .load(url, mImage);
        }

        bt_review.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), List_comment_userActivity.class);

                getApplicationContext().startActivity(intent);
            }
        });
        bt_Edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Edit_UserActivity.class);
                
                getApplicationContext().startActivity(intent);
            }
        });
        bt_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Logout();
            }
        });
        bt_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), AddMovieActivity.class);
                getApplicationContext().startActivity(intent);
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void getvalue(String guid) {
        api_link api = new CallServer().Call();
        Call<Users> call = api.User_Detail(new Users.Users_Input(guid));
        call.enqueue(new Callback<Users>() {
            @Override
            public void onResponse(Call<Users> call, Response<Users> response) {
                tvname.setText(response.body().getName());
                item = response.body();
                tvname.setText(item.getName());
            }

            @Override
            public void onFailure(Call<Users> call, Throwable t) {

            }
        });
}
    private void Logout(){
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = settings.edit();
        editor.clear();
        editor.commit();
        api_link api = new CallServer().Call();
        Call<Review.output> call = api.Logout(new Users.Users_Input(Guid));
        call.enqueue(new Callback<Review.output>() {
            @Override
            public void onResponse(Call<Review.output> call, Response<Review.output> response) {

            }

            @Override
            public void onFailure(Call<Review.output> call, Throwable t) {

            }
        });
        Intent myIntent = new Intent(User.this, MainActivity.class);
        // myIntent.putExtra("key", value);
        User.this.startActivity(myIntent);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
