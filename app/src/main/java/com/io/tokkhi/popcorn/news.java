package com.io.tokkhi.popcorn;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.io.tokkhi.popcorn.API.CallServer;
import com.io.tokkhi.popcorn.API.api_link;
import com.io.tokkhi.popcorn.Adapter.MovieAdapter;
import com.io.tokkhi.popcorn.Adapter.NewsAdapter;
import com.io.tokkhi.popcorn.model.Movie;
import com.io.tokkhi.popcorn.model.News;
import com.io.tokkhi.popcorn.model.Player;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class news extends android.support.v4.app.Fragment {

    private ArrayList<News.item> contactList;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_home, container, false);
        final RecyclerView rv = v.findViewById(R.id.my_recycler_view);
        api_link api = new CallServer().Call();
        Call<News> call = api.News();
        contactList = new ArrayList<>();
        call.enqueue(new Callback<News>() {
            @Override
            public void onResponse(Call<News> call, Response<News> response) {
                contactList = response.body().getNews();
                int numberOfColumns = 2;

                rv.setLayoutManager(new GridLayoutManager(getContext(),numberOfColumns));
                rv.setAdapter(new NewsAdapter(getContext(),contactList));


            }

            @Override
            public void onFailure(Call<News> call, Throwable t) {

            }
        });

        return v;
    }

}
