package com.io.tokkhi.popcorn;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class webviewActivity extends AppCompatActivity  {
    WebView myWebView;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        myWebView = (WebView) findViewById(R.id.webview);
       // myWebView = new WebView(this);
        Intent intent = getIntent();
        String url = intent.getStringExtra("url");
        myWebView.loadUrl(url);
        myWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onLoadResource(WebView view, String url)
            {
                myWebView.loadUrl("javascript:(function() { " +
                        "document.getElementById('header').style.display='none';})()");
                myWebView.loadUrl("javascript:(function() {"+
                        " document.getElementsByClassName('showtime-bar')[0].style.display='none';})()");
                myWebView.loadUrl("javascript:(function() {"+
                        " document.body.style.paddingTop = '0px';})()");
                myWebView.loadUrl("javascript:(function() {"+
                        " document.getElementsByTagName('iframe')[0].style.width = '100%'})()");
            }
        });
        WebSettings webSettings = myWebView.getSettings();

        webSettings.setJavaScriptEnabled(true);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
