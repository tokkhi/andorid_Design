package com.io.tokkhi.popcorn.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.io.tokkhi.popcorn.R;
import com.io.tokkhi.popcorn.detail_move;
import com.io.tokkhi.popcorn.model.Item;

import java.util.ArrayList;

import Helper.DownLoadImageTask;

/**
 * Created by Tokkhi on 2/15/2018.
 */

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.ViewHolder> {

    private static ArrayList<Item.movie_list> mPlayers;
    private Context mContext;
    private LayoutInflater mInflater;
    private Item.movie_list movie;

    public static class ViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener {

        public TextView mName;
        public ImageView mImage;
        public ProgressBar pb;

        public ViewHolder(View view) {
            super(view);
            mName = (TextView) view.findViewById(R.id.tvName);
            mImage = (ImageView) view.findViewById(R.id.itemImage);
            pb = view.findViewById(R.id.progressBar1);
            view.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {

            Intent intent = new Intent(v.getContext(), detail_move.class);
            intent.putExtra("id", mPlayers.get(getAdapterPosition()).getId());
            v.getContext().startActivity(intent);
            Toast.makeText(v.getContext(), "Name = " + mPlayers.get(getAdapterPosition()).getName(), Toast.LENGTH_SHORT).show();

        }
    }

    public MovieAdapter(Context context, ArrayList<Item.movie_list> dataset) {
        mPlayers = dataset;
        mContext = context;

    }


    @Override
    public MovieAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.item_movie, parent, false);
        return  new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        movie = mPlayers.get(position);

        viewHolder.mName.setText(movie.getName().toString());
        viewHolder.mImage.setVisibility(View.GONE);
        viewHolder.pb.setVisibility(View.VISIBLE);
        String url_img = mContext.getResources().getString(R.string.url_img) + movie.getimg();
        new DownLoadImageTask(viewHolder.mImage,viewHolder.pb).execute(url_img);

    }

    @Override
    public int getItemCount() {
        return mPlayers.size();
    }


}