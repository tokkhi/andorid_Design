package com.io.tokkhi.popcorn;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ahmadrosid.svgloader.SvgLoader;
import com.io.tokkhi.popcorn.API.CallServer;
import com.io.tokkhi.popcorn.API.api_link;
import com.io.tokkhi.popcorn.model.Review;
import com.io.tokkhi.popcorn.model.Users;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Edit_UserActivity extends AppCompatActivity {

    private EditText et_name;
    private EditText et_password;
    private Button bt_submit;
    private ImageView im;
    private SharedPreferences get;
    private Button bt_ed_pass;
    private String Guid;
    private EditText et_con;
    private EditText et_n_pass;
    private EditText et_pass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_user);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        et_name = (EditText) findViewById(R.id.et_name);
        et_password = (EditText) findViewById(R.id.et_Password);
        bt_ed_pass = (Button) findViewById(R.id.bt_edit_Pass);


        get = PreferenceManager.getDefaultSharedPreferences(this);

         Guid = get.getString("Guid", null);
        final String name = get.getString("Name",null);
        final String img = get.getString("img",null);
        final String password = get.getString("password",null);

        et_name.setText(name);


        im = (ImageView) findViewById(R.id.imageView1);

        bt_submit = (Button) findViewById(R.id.bt_edit);

        if(img != null){
            String url = getResources().getString(R.string.Url_svg) + img;
            SvgLoader.pluck()
                    .with(this)
                    .setPlaceHolder(R.mipmap.ic_launcher, R.mipmap.ic_launcher)
                    .load(url, im);
        }
        im.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), List_AvatarActivity.class);
                getApplicationContext().startActivity(intent);
            }
        });

        bt_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = get.edit();
                editor.putString("Name", et_name.getText().toString());
                editor.commit();

                api_link api = new CallServer().Call();
                Call<Review.output> call = api.Edit_User(new Users.Edit_Input(Guid, et_name.getText().toString(), img));
                call.enqueue(new Callback<Review.output>() {
                    @Override
                    public void onResponse(Call<Review.output> call, Response<Review.output> response) {
                        String msg = response.body().getReturn_message();
                        if(msg.equals("Success")){
                            Intent myIntent = new Intent(getApplicationContext(), User.class);
                            // myIntent.putExtra("key", value);
                            getApplication().startActivity(myIntent);
                        }


                    }

                    @Override
                    public void onFailure(Call<Review.output> call, Throwable t) {

                    }

                });


            }
        });
        bt_ed_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChangePass();
            }
        });

    }

    private void ChangePass() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.popup_edit_pass);
        dialog.setCancelable(true);

        et_n_pass = (EditText) dialog.findViewById(R.id.et_new_Password);
        et_pass = (EditText) dialog.findViewById(R.id.et_Password);
        et_con = (EditText) dialog.findViewById(R.id.et_Passwordcon);
        Button btc = dialog.findViewById(R.id.submit);
        final String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        btc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String password = get.getString("password",null);
              if(et_pass.getText().toString() != password){
                  Toast.makeText(Edit_UserActivity.this, "รหัสผ่านไม่ถูกต้อง", Toast.LENGTH_SHORT).show();
              }
              else if(!et_n_pass.getText().toString().equals(et_con.getText().toString())){
                  et_n_pass.setError("รหัสผ่านไม่ตรงกัน");
                }else {
                    if(et_n_pass.getText().toString().equals(et_con.getText().toString())){
                        api_link api = new CallServer().Call();
                        Call<Review.output> call = api.Edit_Pass(new Users.Edit_Pass(Guid, et_con.getText().toString()));
                        call.enqueue(new Callback<Review.output>() {
                            @Override
                            public void onResponse(Call<Review.output> call, Response<Review.output> response) {

                                String msg = response.body().getReturn_message();
                                if(msg.equals("Success")){
                                    Intent myIntent = new Intent(getApplicationContext(), User.class);
                                    // myIntent.putExtra("key", value);
                                    getApplication().startActivity(myIntent);
                                }
                            }

                            @Override
                            public void onFailure(Call<Review.output> call, Throwable t) {

                            }

                        });
                    }else{

                    }
                }





            }
        });
        dialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
