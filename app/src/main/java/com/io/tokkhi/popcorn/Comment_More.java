package com.io.tokkhi.popcorn;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.io.tokkhi.popcorn.Adapter.CommentAdapter;
import com.io.tokkhi.popcorn.model.Item;

import java.util.ArrayList;

public class Comment_More extends AppCompatActivity {

    private RecyclerView mrecyclerView ;
    ArrayList<Item.Comment> mitem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment__more);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Intent intent = getIntent();
        mitem = (ArrayList<Item.Comment>) intent.getSerializableExtra("item");
        mrecyclerView = findViewById(R.id.recycler_view);
        CommentAdapter itemListDataAdapter = new CommentAdapter(this, mitem,999);
        mrecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mrecyclerView.setAdapter(itemListDataAdapter);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
