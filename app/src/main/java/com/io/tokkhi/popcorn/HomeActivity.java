package com.io.tokkhi.popcorn;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.io.tokkhi.popcorn.API.CallServer;
import com.io.tokkhi.popcorn.API.api_link;
import com.io.tokkhi.popcorn.Adapter.HomeAdapter;
import com.io.tokkhi.popcorn.model.Director;
import com.io.tokkhi.popcorn.model.Home;
import com.io.tokkhi.popcorn.model.SectionDataModel;
import com.io.tokkhi.popcorn.model.SingleItemModel;

import java.util.ArrayList;
import java.util.Collection;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends android.support.v4.app.Fragment {
    ArrayList<SectionDataModel> allSampleData;
    private Button add;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_home, container, false);
        final ArrayList<Home> item = new ArrayList<>();
        final RecyclerView rv = (RecyclerView)v.findViewById(R.id.my_recycler_view);
        api_link api = new CallServer().Call();
        Call<Home> call = api.Home();
        call.enqueue(new Callback<Home>() {
                         @Override
                         public void onResponse(Call<Home> call, Response<Home> response) {
                            Home Data = response.body();
                            item.add(Data);
                           if(item.size() > 0) {
                               rv.setLayoutManager(new LinearLayoutManager(getContext()));
                               rv.setAdapter(new HomeAdapter(getContext(), item));
                           }
                         }
                         @Override
                         public void onFailure(Call<Home> call, Throwable t) {

                         }
                     });


        return v;
    }

}
