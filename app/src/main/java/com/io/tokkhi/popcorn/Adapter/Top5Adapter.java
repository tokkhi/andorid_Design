package com.io.tokkhi.popcorn.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.io.tokkhi.popcorn.R;
import com.io.tokkhi.popcorn.detail_move;
import com.io.tokkhi.popcorn.model.Home;
import com.io.tokkhi.popcorn.model.SingleItemModel;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;

import Helper.DownLoadImageTask;

public class Top5Adapter extends RecyclerView.Adapter<Top5Adapter.SingleItemRowHolder> {

    private ArrayList<Home.item_Movie> itemsList;
    private Context mContext;

    public Top5Adapter(Context context, ArrayList<Home.item_Movie> itemsList) {
        this.itemsList = itemsList;
        this.mContext = context;
    }

    @Override
    public Top5Adapter.SingleItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_title_home, null);
        Top5Adapter.SingleItemRowHolder mh = new Top5Adapter.SingleItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(Top5Adapter.SingleItemRowHolder holder, int i) {
        Home.item_Movie items = itemsList.get(i);
        holder.tvName.setText(items.getName_Movie());
        holder.tvDate.setText("วันที่ : "+items.getDate());
        holder.tv_master.setText(items.getScore_Master().toString());
        holder.tv_User.setText(items.getScore_Users().toString());
        holder.imageMovie.setVisibility(View.GONE);
        holder.pb.setVisibility(View.VISIBLE);
        String url_img = mContext.getResources().getString(R.string.url_img) + items.getImg();
        new Helper.DownLoadImageTask(holder.imageMovie,holder.pb).execute(url_img);
        if(items.getScore_Master() > 7){
            holder.img_master.setImageResource(R.drawable.ic_master_good);
        }else if(items.getScore_Master() > 3){
            holder.img_master.setImageResource(R.drawable.ic_master_fine);
        }else{
            holder.img_master.setImageResource(R.drawable.ic_master_bad);
        }
        if(items.getScore_Users() > 4){
            holder.img_user.setImageResource(R.drawable.ic_user_good);
        }else if(items.getScore_Users()  > 2.5){
            holder.img_user.setImageResource(R.drawable.ic_user_find);
        }else{
            holder.img_user.setImageResource(R.drawable.ic_user_bad);
        }
    }

    @Override
    public int getItemCount() {
        return (null != itemsList ? itemsList.size() : 0);
    }

    public class SingleItemRowHolder extends RecyclerView.ViewHolder {

        private final TextView tv_master;
        private final TextView tv_User;
        private final ImageView img_master;
        private final ImageView img_user;
        public TextView tvName;
        private TextView tvDate;
        private ProgressBar pb;

        public RoundedImageView imageMovie;


        public SingleItemRowHolder(View view) {
            super(view);

            tvName = (TextView) view.findViewById(R.id.Name);
            tvDate = (TextView) view.findViewById(R.id.Date);
            imageMovie = (RoundedImageView) view.findViewById(R.id.imageMovie);
            tv_master = (TextView) view.findViewById(R.id.tv_master);
            tv_User = (TextView) view.findViewById(R.id.tv_user);
            img_master = (ImageView) view.findViewById(R.id.img_master);
            img_user = (ImageView) view.findViewById(R.id.img_user);
            pb = view.findViewById(R.id.progressBar1);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(v.getContext(), detail_move.class);
                    intent.putExtra("id", itemsList.get(getAdapterPosition()).getID_Movie());
                    v.getContext().startActivity(intent);

                    Toast.makeText(v.getContext(),"", Toast.LENGTH_SHORT).show();


                }
            });


        }

    }

}