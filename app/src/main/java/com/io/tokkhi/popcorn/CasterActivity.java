package com.io.tokkhi.popcorn;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.io.tokkhi.popcorn.API.CallServer;
import com.io.tokkhi.popcorn.API.api_link;
import com.io.tokkhi.popcorn.Adapter.CasterAdapter;
import com.io.tokkhi.popcorn.model.Caster;
import com.io.tokkhi.popcorn.model.Item;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CasterActivity extends android.support.v4.app.Fragment {

    private List<Item> allSampleData;
    private EditText et;
    private Button bt;
    private ArrayList<Caster.item_list> contactList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_cast, container, false);
        final RecyclerView rv = (RecyclerView)v.findViewById(R.id.my_recycler_view);
        api_link api = new CallServer().Call();
        Call<Caster> call = api.Caster();
        contactList = new ArrayList<>();
        call.enqueue(new Callback<Caster>() {
            @Override
            public void onResponse(Call<Caster> call, Response<Caster> response) {

                int numberOfColumns = 2;

                rv.setLayoutManager(new GridLayoutManager(getContext(),numberOfColumns));
                rv.setAdapter(new CasterAdapter(getContext(),response.body().getCaster()));
            }
            @Override
            public void onFailure(Call<Caster> call, Throwable t) {

            }
        });
        return v;

}
    }
