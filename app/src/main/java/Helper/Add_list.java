package Helper;

import android.os.AsyncTask;

import com.io.tokkhi.popcorn.model.Caster;
import com.io.tokkhi.popcorn.model.Director;
import com.io.tokkhi.popcorn.model.Home;
import com.io.tokkhi.popcorn.model.Item;
import com.io.tokkhi.popcorn.model.SectionDataModel;
import com.io.tokkhi.popcorn.model.SingleItemModel;

import java.util.ArrayList;

/**
 * Created by Tokkhi on 3/21/2018.
 */

public class  Add_list {
    public ArrayList<SingleItemModel> Add_Caster(ArrayList<Item.Caster> input){
        ArrayList<SingleItemModel> singleItem = new ArrayList<>();

            for (int i = 0; i < input.size(); i++) {
                singleItem.add(new SingleItemModel(input.get(i).getName(), input.get(i).getId(),input.get(i).getImg()));
        }
        return singleItem;
    }
    public ArrayList<SingleItemModel> Add_Directer(ArrayList<Item.Director> input){
        ArrayList<SingleItemModel> singleItem = new ArrayList<>();

        for (int i = 0; i < input.size(); i++) {
            singleItem.add(new SingleItemModel(input.get(i).getName(), input.get(i).getId(),input.get(i).getImg()));
        }
        return singleItem;
    }
    public ArrayList<SingleItemModel> Add_Top5(ArrayList<Home.item_Movie> input){
        ArrayList<SingleItemModel> singleItem = new ArrayList<>();

        for (int i = 0; i < input.size(); i++) {
            singleItem.add(new SingleItemModel(input.get(i).getName_Movie(), input.get(i).getID_Movie(),input.get(i).getImg()));
        }
        return singleItem;
    }
    public ArrayList<SingleItemModel> D_Movie(ArrayList<Director.item.movie> input){
        ArrayList<SingleItemModel> singleItem = new ArrayList<>();

        for (int i = 0; i < input.size(); i++) {
            singleItem.add(new SingleItemModel(input.get(i).getName(), input.get(i).getId(),input.get(i).getImg()));
        }
        return singleItem;
    }


}
