package Helper;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.io.tokkhi.popcorn.R;

import java.io.InputStream;
import java.net.URL;

/**
 * Created by Tokkhi on 3/19/2018.
 */

public class DownLoadImageTask extends AsyncTask<String,Void,Bitmap> {
    ImageView imageView;
    ProgressBar pb = null;


    public DownLoadImageTask(ImageView imageView,ProgressBar pb){

        this.imageView = imageView;
        this.pb = pb;

    }
    public DownLoadImageTask(ImageView imageView){

        this.imageView = imageView;
    }


    @Override
    protected Bitmap doInBackground(String... strings) {

        String urlOfImage = strings[0];
        Bitmap logo = null;

        try{
            InputStream is = new  URL(urlOfImage).openStream();

            logo = BitmapFactory.decodeStream(is);
            logo = Bitmap.createScaledBitmap(logo, 550, 700, true);

        }catch(Exception e){ // Catch the download exception
            e.printStackTrace();
        }
        return logo;
    }
    protected void onPostExecute(Bitmap result){
        imageView.setVisibility(View.VISIBLE);
        if(pb != null){
            pb.setVisibility(View.GONE);  //
        }

        imageView.setImageBitmap(result);
    }
}
