package Helper;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.pchmn.materialchips.model.Chip;

/**
 * Created by Tokkhi on 3/24/2018.
 */

public class Tag extends Chip {
    private String mName;
    private int mType = 0;

    public Tag(String name, int type) {
        super(name, String.valueOf(type));
        mType = type;
        mName = name;
    }




    public String getText() {
        return mName;
    }

    public int getType() {
        return mType;
    }
}